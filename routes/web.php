<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/welcome', 'HomeController@welcome')
    ->name('welcome'); 

Route::get('/', 'HomeController@index')
    ->name('home'); 

Route::prefix('list')
    ->middleware(['auth', 'active', 'verified'])
    ->group(function() {
        Route::get('/{id}', 'DetailController@indexList')
            ->name('indexList');
        Route::get('/indexDetail/{agreement_no}', 'DetailController@indexDetail')
            ->name('indexDetail');  
        Route::get('/showCheckOut/{id_transactions}', 'DetailController@showCheckOut')
            ->name('showCheckOut');
        Route::get('/showCheckList/{id_transactions}', 'DetailController@showCheckList')
            ->name('showCheckList');
        Route::post('/addCheckList', 'DetailController@addCheckList')
            ->name('addCheckList');
        Route::post('/addNegotiation', 'DetailController@addNegotiation')
            ->name('addNegotiation');
        Route::post('/addBuyNow', 'DetailController@addBuyNow')
            ->name('addBuyNow');
    });

Route::get('/showCheckOutSuccess/{id_transactions}', 'CheckoutController@showCheckOutSuccess')
    ->middleware(['auth', 'active', 'verified'])
    ->name('showCheckOutSuccess');

Route::get('/order', 'OrderController@index')
    ->middleware(['auth', 'active', 'verified'])
    ->name('order'); 

Route::prefix('admin')
    ->middleware(['auth', 'admin', 'verified'])
    ->group(function() {
        Route::get('/', 'Admin\DashboardController@index')
            ->name('admin');
            
        Route::prefix('bidder')
            ->group(function() {
                Route::get('/', 'Admin\BidderController@index')
                    ->name('bidder');
                Route::get('/edit/{id}', 'Admin\BidderController@edit')
                    ->name('edit-bidder');
                Route::post('/update', 'Admin\BidderController@update')
                    ->name('update-bidder');
                Route::post('/approveBidder/{id}', 'Admin\BidderController@approveBidder')
                    ->name('approveBidder');
                Route::get('/destroy/{id}', 'Admin\BidderController@destroy')
                    ->name('destroy-bidder');
            });

        Route::prefix('warehouse')
            ->group(function() {
                Route::get('/', 'Admin\WarehouseController@index')
                    ->name('warehouse');
                Route::get('/create', 'Admin\WarehouseController@create')
                    ->name('create-warehouse');
                Route::post('/add', 'Admin\WarehouseController@add')
                    ->name('add-warehouse');
                Route::get('/edit/{id}', 'Admin\WarehouseController@edit')
                    ->name('edit-warehouse');
                Route::post('/update', 'Admin\WarehouseController@update')
                    ->name('update-warehouse');
                Route::get('/destroy/{id}', 'Admin\WarehouseController@destroy')
                    ->name('destroy-warehouse');
            });

        Route::prefix('asset')
            ->group(function() {
                Route::get('/', 'Admin\AssetController@index')
                    ->name('asset');
                Route::get('/indexReject', 'Admin\AssetController@indexReject')
                    ->name('indexReject');
                Route::get('/show/{id}', 'Admin\AssetController@show')
                    ->name('asset-show');
                Route::post('/import', 'Admin\AssetController@import')
                    ->name('import-asset');
                Route::get('/manageAsset', 'Admin\AssetController@manageAsset')
                    ->name('manageAsset');
                Route::get('/managePhoto/{id}', 'Admin\AssetController@managePhoto')
                    ->name('managePhoto');
                Route::post('/addPhoto', 'Admin\AssetController@addPhoto')
                    ->name('addPhoto');
                Route::get('/deletePhoto/{id_photos}', 'Admin\AssetController@deletePhoto')
                    ->name('deletePhoto');
                Route::post('/ajustPrice', 'Admin\AssetController@ajustPrice')
                    ->name('ajustPrice');
                Route::get('/edit/{id}', 'Admin\AssetController@edit')
                    ->name('edit-asset');
                Route::post('/update', 'Admin\AssetController@update')
                    ->name('update-asset');
                Route::get('/destroy/{id}', 'Admin\AssetController@destroy')
                    ->name('destroy-asset');
            });

        Route::prefix('event-management')
            ->group(function() {
                Route::get('/', 'Admin\EventManagementController@index')
                    ->name('event-management');
                Route::get('/eventDetail/{id}', 'Admin\EventManagementController@eventDetail')
                    ->name('eventDetail');
                Route::get('/createManagement', 'Admin\EventManagementController@createManagement')
                    ->name('createManagement');
                Route::post('/addManagement', 'Admin\EventManagementController@addManagement')
                    ->name('addManagement');
                Route::get('exportReport', 'Admin\EventManagementController@exportReport')
                    ->name('exportReport');
                Route::get('/editManagement/{id}', 'Admin\EventManagementController@editManagement')
                    ->name('editManagement');
                Route::post('/updateManagement', 'Admin\EventManagementController@updateManagement')
                    ->name('updateManagement');
                Route::get('/destroyManagement/{id}', 'Admin\EventManagementController@destroyManagement')
                    ->name('destroyManagement');
            });
        
        Route::prefix('transaction')
            ->group(function() {
                Route::get('/', 'Admin\TransactionController@indexTransaction')
                    ->name('indexTransaction');
                Route::get('/indexNegotiation', 'Admin\TransactionController@indexNegotiation')
                    ->name('indexNegotiation');
                Route::get('/negoDetail/{agreement_assets}', 'Admin\TransactionController@negoDetail')
                    ->name('negoDetail');
                Route::post('/processNegotiation', 'Admin\TransactionController@processNegotiation')
                    ->name('processNegotiation');
                Route::post('/rejectNegotiation', 'Admin\TransactionController@rejectNegotiation')
                    ->name('rejectNegotiation');
            });

    });

Route::get('/OpenHouseUpdate/{id}', 'Admin\EventManagementController@OpenHouseUpdate')
    ->name('OpenHouseUpdate');

Route::prefix('register')
    ->group(function() {
        Route::get('/createData', 'Auth\RegisterController@createData')
            ->name('createData');
        Route::post('/addData', 'Auth\RegisterController@addData')
            ->name('addData');
        Route::get('/createOTP', 'Auth\RegisterController@createOTP')
            ->name('createOTP');
        Route::post('/inputOTP', 'Auth\RegisterController@inputOTP')
            ->name('inputOTP');
    });

Route::get('refreshCaptcha', 'Auth\LoginController@refreshCaptcha')
    ->name('refreshCaptcha');

Route::get('logout', 'Auth\LoginController@logout')
    ->name('logout');

Auth::routes(['verify' => true]);