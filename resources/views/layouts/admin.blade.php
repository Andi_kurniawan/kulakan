<!DOCTYPE html>
<html lang="en" class="no-js">

<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>KULAKAN Admin</title>

    @stack('prepend-style')

    @include('includes.admin.style')

</head>

<body>

    @include('includes.admin.navbar')

    @yield('content')

    @include('includes.admin.footer')

    @include('includes.admin.script')

    @stack('addon-script')

</body>

</html>