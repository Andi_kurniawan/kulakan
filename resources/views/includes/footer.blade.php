<!-- FOOTER -->
<footer class="section-footer mt-5 mb-5 border-top">
    <div class="container pt-5 pb-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="row">
                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <img src="{{ url('frontend/images/logo-footer.png') }}" alt="" class="img-footer">
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <h5>LINK</h5>
                        <ul class="list-unstyled">
                            <li><a href="#">Career</a></li>
                            <li><a href="#">Help Center</a></li>
                            <li><a href="#">Media</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <h5>OFFICE'S</h5>
                        <ul class="list-unstyled">
                            <li><a href="https://sitama.co.id">PT. Solusi Integrasi Pratama</a></li>
                            <br>
                            <li><a href="#">BFI Tower, Jl. Kapten Soebijanto Djojohadikusumo Bsd City, Tangerang Selatan - 15322</a></li>
                            <br>
                            <li><a href="#">(021) 296 50555</a></li>
                            <br>
                            <li><a href="#">contact@kulakan.com</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row border-top justify-content-center align-items-center pt-4">
            <div class="col-auto text-gray-500 font-weight-light">
                <p>2020 Copyright KULAKAN • All rights reserved</p>
            </div>
        </div>
    </div>
</footer>