<!-- NAVBAR -->
<div class="container">
    <nav class="row navbar navbar-expand-lg navbar-light">
        <a href="{{ route('home') }}" class="navbar-brand">
            <img src="{{ url('frontend/images/logo.png') }}" alt="Logo NOMADS" />
        </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navb">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navb">

            @guest
                <ul class="navbar-nav ml-auto mr-3">
                    <li class="nav-item mx-md-2">
                        <a href="{{ route('home') }}" class="nav-link">Beranda</a>
                    </li>
                    <li class="nav-item mx-md-2">
                        <a href="{{ route('order') }}" class="nav-link">Pesanan</a>
                    </li>
                    {{-- <li class="nav-item mx-md-2">
                        <a href="#" class="nav-link">Keranjang</a>
                    </li> --}}
                </ul>
            
                <!-- Mobile Button -->
                <form class="form-inline d-sm-block d-md-none">
                    @csrf
                    <button class="btn btn-login my-2 my-sm-0" type="button" 
                    onclick="event.preventDefault();location.href='{{ url('login') }}';">
                        Masuk
                    </button>
                    <button class="btn btn-login my-2 my-sm-0 ml-3" type="button" 
                    onclick="event.preventDefault();location.href='{{ url('register') }}';">
                        Daftar
                    </button>
                </form>

                <!-- Dekstop Button -->
                <form class="form-inline my-2 my-lg-0 d-none d-md-block">
                    @csrf
                    <button class="btn btn-login btn-navbar-right my-2 my-sm-0 px-4" type="button" 
                    onclick="event.preventDefault();location.href='{{ url('login') }}';">
                        Masuk
                    </button>
                    <button class="btn btn-login btn-navbar-right my-2 my-sm-0 px-4" type="button" 
                    onclick="event.preventDefault();location.href='{{ url('register') }}';">
                        Daftar
                    </button>
                </form>
            @endguest
            
            @auth
                <ul class="navbar-nav ml-auto mr-3">
                    <li class="nav-item mx-md-2 {{ (request()->is('/')) ? 'active' : '' }}">
                        <a href="{{ route('home') }}" class="nav-link">Beranda</a>
                    </li>
                    <li class="nav-item mx-md-2 {{ (request()->is('order')) ? 'active' : '' }}">
                        <a href="{{ route('order') }}" class="nav-link">Pesanan</a>
                    </li>
                    {{-- <li class="nav-item mx-md-2">
                        <a href="#" class="nav-link">Keranjang</a>
                    </li> --}}
                    <li class="nav-item dropdown">
                        <a href="" class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown">
                            Akun
                        </a>
                        <div class="dropdown-menu">
                            <a href="#" class="dropdown-item">
                                <img src="https://ui-avatars.com/api/?name={{ Auth::user()->name }}" height="40"
                                    class="rounded-circle" style="margin-right:5px;"/> 
                                {{ Auth::user()->name }}
                            </a>
                            <hr>
                            <a href="#" class="dropdown-item">Dashboard</a>
                            <a href="{{ route('logout') }}" class="dropdown-item">Keluar</a>
                        </div>
                    </li>
                </ul>
            @endauth
            
        </div>
    </nav>
</div>