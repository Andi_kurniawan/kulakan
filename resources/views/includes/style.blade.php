<link rel="stylesheet" href="{{ url('frontend/libraries/bootstrap/css/bootstrap.css') }}" />
<link rel="stylesheet" href="{{ url('frontend/libraries/font-awesome.css') }}" />
<link
    href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Saira+Condensed:100,200,300,400,500,600,700,800,900|Saira:100,200,300,400,500,600,700,800,900&display=swap"
    rel="stylesheet">
<link rel="stylesheet" href="{{ url('frontend/styles/main.css') }}" />