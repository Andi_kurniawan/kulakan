<div class="header-top-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="logo-area">
                    <a href="{{ route('admin') }}">
                        <h2 style="
                            font-family:'Saira Semi Condensed', sans-serif;
                            font-weight:600;
                            font-size:36px;">
                            Admin KULAKAN
                        </h2>
                    </a>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="header-top-menu">
                    <ul class="nav navbar-nav notika-top-nav">
                        <li class="nav-item dropdown">
                            <a href="#" data-toggle="dropdown" role="button" aria-expanded="false"
                                class="nav-link dropdown-toggle"><span><i
                                        class="notika-icon notika-search"></i></span></a>
                            <div role="menu" class="dropdown-menu search-dd animated flipInX">
                                <div class="search-input">
                                    <i class="notika-icon notika-left-arrow" style="color:#000;"></i>
                                    <input type="text" />
                                </div>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="#" data-toggle="dropdown" role="button" aria-expanded="false"
                                class="nav-link dropdown-toggle"><span><i
                                        class="notika-icon notika-user"></i></span></a>
                            <div role="menu" class="dropdown-menu message-dd animated zoomIn">
                                <div class="hd-mg-tt">
                                    <h2>Account</h2>
                                </div>
                                <div class="hd-message-info">
                                    <a href="#">
                                        <div class="hd-message-sn">
                                            <div class="hd-mg-ctn">
                                                <h3>{{ Auth::user()->name }}</h3>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="{{ route('logout') }}">
                                        <div class="hd-message-sn">
                                            <div class="hd-mg-ctn">
                                                <h3>Logout</h3>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="mobile-menu-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="mobile-menu">
                    <nav id="dropdown">
                        <ul class="mobile-menu-nav">
                            <li><a data-toggle="collapse" data-target="#Charts" href="{{ route('admin') }}">Home</a>
                            </li>
                            <li><a data-toggle="collapse" data-target="#demoevent"
                                    href="{{ route('bidder') }}">Bidder</a>
                            </li>
                            <li><a data-toggle="collapse" data-target="#democrou"
                                    href="{{ route('warehouse') }}">Warehouse</a>
                            </li>
                            <li><a data-toggle="collapse" data-target="#demolibra" href="{{ route('asset') }}">Asset</a>
                            </li>
                            <li><a data-toggle="collapse" data-target="#demodepart"
                                    href="{{ route('event-management') }}">Event</a>
                            </li>
                            <li><a data-toggle="collapse" data-target="#demo"
                                    href="{{ route('indexNegotiation') }}">Negotiation</a>
                            </li>
                            <li><a data-toggle="collapse" data-target="#Miscellaneousmob"
                                    href="{{ route('indexTransaction') }}">Transaction</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="main-menu-area mg-tb-40">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">
                    <li class="{{ (request()->is('admin')) ? 'active' : '' }}">
                        <a href="{{ route('admin') }}" style="font-size:16px;"><i class="notika-icon notika-house"></i>
                            Home</a>
                    </li>
                    <li class="{{ (request()->is('admin/bidder')) ? 'active' : '' }} 
                        {{ (request()->is('admin/bidder/edit/{id}')) ? 'active' : '' }}">
                        <a href="{{ route('bidder') }}" style="font-size:16px;"><i class="notika-icon notika-mail"></i>
                            Bidder</a>
                    </li>
                    <li class="{{ (request()->is('admin/warehouse')) ? 'active' : '' }}">
                        <a href="{{ route('warehouse') }}" style="font-size:16px;"><i
                                class="notika-icon notika-edit"></i> Warehouse</a>
                    </li>
                    <li class="{{ (request()->is('admin/asset')) ? 'active' : '' }}">
                        <div class="dropdown-trig-sgn">
                            <button class="triger-fadeIn btn-asset" data-toggle="dropdown"><i
                                    class="notika-icon notika-bar-chart"></i>Asset</button>
                            <ul class="dropdown-menu triger-fadeIn-dp">
                                <li class="{{ (request()->is('admin/asset')) ? 'active' : '' }}">
                                    <a href="{{ route('asset') }}">Asset Available</a>
                                </li>
                                <li class="{{ (request()->is('admin/indexReject')) ? 'active' : '' }}">
                                    <a href="{{ route('indexReject') }}">Asset Rejected</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="{{ (request()->is('admin/event-management')) ? 'active' : '' }}">
                        <a href="{{ route('event-management') }}" style="font-size:16px;"><i
                                class="notika-icon notika-windows"></i> Event</a>
                    </li>
                    <li class="{{ (request()->is('admin/transaction/indexNegotiation')) ? 'active' : '' }}">
                        <a href="{{ route('indexNegotiation') }}" style="font-size:16px;"><i
                                class="notika-icon notika-form"></i> Negotiation</a>
                    </li>
                    <li class="{{ (request()->is('admin/transaction')) ? 'active' : '' }}">
                        <a href="{{ route('indexTransaction') }}" style="font-size:16px;"><i
                                class="notika-icon notika-app"></i> Transaction</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <form action="{{ url('logout') }}" method="POST">
                    @csrf

                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary" type="submit">Logout</button>
                </form>
            </div>
        </div>
    </div>
</div>