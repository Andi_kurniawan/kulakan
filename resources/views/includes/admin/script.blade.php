<!-- Bootstrap core JavaScript-->
<script src="{{ url('backend/js/vendor/modernizr-2.8.3.min.js') }}"></script>

<!-- jquery
		============================================ -->
<script src="{{ url('backend/js/vendor/jquery-1.12.4.min.js') }}"></script>
<!-- bootstrap JS
            ============================================ -->
<script src="{{ url('backend/js/bootstrap.min.js') }}"></script>
<!-- wow JS
            ============================================ -->
<script src="{{ url('backend/js/wow.min.js') }}"></script>
<!-- price-slider JS
            ============================================ -->
<script src="{{ url('backend/js/jquery-price-slider.js') }}"></script>
<!-- owl.carousel JS
            ============================================ -->
<script src="{{ url('backend/js/owl.carousel.min.js') }}"></script>
<!-- scrollUp JS
            ============================================ -->
<script src="{{ url('backend/js/jquery.scrollUp.min.js') }}"></script>
<!-- meanmenu JS
            ============================================ -->
<script src="{{ url('backend/js/meanmenu/jquery.meanmenu.js') }}"></script>
<!-- counterup JS
            ============================================ -->
<script src="{{ url('backend/js/counterup/jquery.counterup.min.js') }}"></script>
<script src="{{ url('backend/js/counterup/waypoints.min.js') }}"></script>
<script src="{{ url('backend/js/counterup/counterup-active.js') }}"></script>
<!-- mCustomScrollbar JS
            ============================================ -->
<script src="{{ url('backend/js/scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<!-- jvectormap JS
            ============================================ -->
<script src="{{ url('backend/js/jvectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
<script src="{{ url('backend/js/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ url('backend/js/jvectormap/jvectormap-active.js') }}"></script>
<!-- sparkline JS
            ============================================ -->
<script src="{{ url('backend/js/sparkline/jquery.sparkline.min.js') }}"></script>
<script src="{{ url('backend/js/sparkline/sparkline-active.js') }}"></script>
<!-- sparkline JS
            ============================================ -->
<script src="{{ url('backend/js/flot/jquery.flot.js') }}"></script>
<script src="{{ url('backend/js/flot/jquery.flot.resize.js') }}"></script>
<script src="{{ url('backend/js/flot/curvedLines.js') }}"></script>
<script src="{{ url('backend/js/flot/flot-active.js') }}"></script>
<!-- knob JS
            ============================================ -->
<script src="{{ url('backend/js/knob/jquery.knob.js') }}"></script>
<script src="{{ url('backend/js/knob/jquery.appear.js') }}"></script>
<script src="{{ url('backend/js/knob/knob-active.js') }}"></script>
<!--  wave JS
            ============================================ -->
<script src="{{ url('backend/js/wave/waves.min.js') }}"></script>
<script src="{{ url('backend/js/wave/wave-active.js') }}"></script>
<!--  todo JS
            ============================================ -->
<script src="{{ url('backend/js/todo/jquery.todo.js') }}"></script>
<!-- plugins JS
            ============================================ -->
<script src="{{ url('backend/js/plugins.js') }}"></script>
<!--  Chat JS
            ============================================ -->
<script src="{{ url('backend/js/chat/moment.min.js') }}"></script>
<script src="{{ url('backend/js/chat/jquery.chat.js') }}"></script>
<!-- main JS
            ============================================ -->
<script src="{{ url('backend/js/main.js') }}"></script>
<!-- Data Table JS
		============================================ -->
<script src="{{ url('backend/js/data-table/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('backend/js/data-table/data-table-act.js') }}"></script>