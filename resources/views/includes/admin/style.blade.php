<!-- favicon
		============================================ -->
<link rel="shortcut icon" type="image/x-icon" href="{{ url('backend/img/favicon.ico') }}">
<!-- Google Fonts
            ============================================ -->
<link
    href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Saira+Semi+Condensed:100,200,300,400,500,600,700,800,900&display=swap"
    rel="stylesheet">
<!-- Bootstrap CSS
            ============================================ -->
<link rel="stylesheet" href="{{ url('backend/css/bootstrap.min.css') }}">
<!-- Bootstrap CSS
            ============================================ -->
<link rel="stylesheet" href="{{ url('backend/css/font-awesome.min.css') }}">
<!-- owl.carousel CSS
            ============================================ -->
<link rel="stylesheet" href="{{ url('backend/css/owl.carousel.css') }}">
<link rel="stylesheet" href="{{ url('backend/css/owl.theme.css') }}">
<link rel="stylesheet" href="{{ url('backend/css/owl.transitions.css') }}">
<!-- meanmenu CSS
            ============================================ -->
<link rel="stylesheet" href="{{ url('backend/css/meanmenu/meanmenu.min.css') }}">
<!-- animate CSS
            ============================================ -->
<link rel="stylesheet" href="{{ url('backend/css/animate.css') }}">
<!-- normalize CSS
            ============================================ -->
<link rel="stylesheet" href="{{ url('backend/css/normalize.css') }}">
<!-- mCustomScrollbar CSS
            ============================================ -->
<link rel="stylesheet" href="{{ url('backend/css/scrollbar/jquery.mCustomScrollbar.min.css') }}">
<!-- jvectormap CSS
            ============================================ -->
<link rel="stylesheet" href="{{ url('backend/css/jvectormap/jquery-jvectormap-2.0.3.css') }}">
<!-- notika icon CSS
            ============================================ -->
<link rel="stylesheet" href="{{ url('backend/css/notika-custom-icon.css') }}">
<!-- Data Table JS
		============================================ -->
<link rel="stylesheet" href="{{ url('backend/css/jquery.dataTables.min.css') }}">
<!-- wave CSS
            ============================================ -->
<link rel="stylesheet" href="{{ url('backend/css/wave/waves.min.css') }}">
<!-- main CSS
            ============================================ -->
<link rel="stylesheet" href="{{ url('backend/css/main.css') }}">
<!-- style CSS
            ============================================ -->
<link rel="stylesheet" href="{{ url('backend/style.css') }}">
<!-- responsive CSS
            ============================================ -->
<link rel="stylesheet" href="{{ url('backend/css/responsive.css') }}">
<!-- modernizr JS
            ============================================ -->