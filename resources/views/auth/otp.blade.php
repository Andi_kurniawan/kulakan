@extends('layouts.auth')

@section('title', 'OTP KULAKAN')

@section('content')
<div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <div class="row">
                <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
                <div class="col-lg-7">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Masukan OTP!</h1>
                            <p class="mb-4">Masukan otp
                                yang telah kami kirim ke sms / email anda!
                            </p>
                        </div>

                        <form class="user otp-group" method="POST" action="{{ route('addData') }}"
                            data-group-name="otps" data-autosubmit="false" autocomplete="off">
                            @csrf

                            <div class="form-group row justify-content-center">
                                <div class=" col-lg-2 col-md-2 col-sm-2 col-2 px-1" style="max-width:85px;">
                                    <input id="otp" type="text" class="form-control form-control-user text-center"
                                        name="otp-1" data-next="otp-2" required id="otp-1"
                                        style="max-width:75px;height:70px;font-size:32px;" />
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-2 px-1" style="max-width:85px;">
                                    <input id="otp" type="text" class="form-control form-control-user text-center"
                                        name="otp-2" data-next="otp-3" data-previous="otp-1" required id="otp-2"
                                        style="max-width:75px;height:70px;font-size:32px;" />
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-2 px-1" style="max-width:85px;">
                                    <input id="otp" type="text" class="form-control form-control-user text-center"
                                        name="otp-3" data-next="otp-4" data-previous="otp-2" required id="otp-3"
                                        style="max-width:75px;height:70px;font-size:32px;" />
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-2 px-1" style="max-width:85px;">
                                    <input id="otp" type="text" class="form-control form-control-user text-center"
                                        name="otp-4" data-next="otp-5" data-previous="otp-3" required id="otp-4"
                                        style="max-width:75px;height:70px;font-size:32px;" />
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-2 px-1" style="max-width:85px;">
                                    <input id="otp" type="text" class="form-control form-control-user text-center"
                                        name="otp-5" data-next="otp-6" data-previous="otp-4" required id="otp-5"
                                        style="max-width:75px;height:70px;font-size:32px;" />
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-2 px-1" style="max-width:85px;">
                                    <input id="otp" type="text" class="form-control form-control-user text-center"
                                        name="otp-6" data-previous="otp-5" required id="otp-6"
                                        style="max-width:75px;height:70px;font-size:32px;" />
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary btn-user btn-block">
                                {{ __('Input OTP') }}
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('prepend-style')
@endpush

@push('addon-script')
<script>
    $('.otp-group').find('input').each(function() {
        $(this).attr('maxlength', 1);
        $(this).on('keyup', function(e) {
            var parent = $($(this).parent());
            
            if(e.keyCode === 8 || e.keyCode === 37) {
                var prev = parent.find('input#' + $(this).data('previous'));
                
                if(prev.length) {
                    $(prev).select();
                }
            } else if((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
                var next = parent.find('input#' + $(this).data('next'));
                
                if(next.length) {
                    $(next).select();
                } else {
                    if(parent.data('autosubmit')) {
                        parent.submit();
                    }
                }
            }
        });
    });
</script>
@endpush