@extends('layouts.auth')

@section('title', 'Input Data for KULAKAN')

@section('content')
<div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
                <div class="col-lg-7">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Masukan Data!</h1>
                        </div>

                        <form class="user" method="POST" action="{{ route('addData') }}">
                            @csrf

                            <div class="form-group ">
                                <input id="email" type="email"
                                    class="form-control form-control-user @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" required autocomplete="email"
                                    placeholder="Email" readonly>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <input id="name" type="text"
                                    class="form-control form-control-user @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name') }}" required autocomplete="name" autofocus
                                    placeholder="Nama">

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <input id="identity_number" type="text"
                                    class="form-control form-control-user @error('identity_number') is-invalid @enderror"
                                    name="identity_number" value="{{ old('identity_number') }}" required
                                    autocomplete="identity_number" placeholder="No KTP">

                                @error('identity_number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <input id="birthday_place" type="text"
                                        class="form-control form-control-user @error('birthday_place') is-invalid @enderror"
                                        name="birthday_place" value="{{ old('birthday_place') }}" required
                                        autocomplete="birthday_place" placeholder="Tempat Lahir">

                                    @error('place')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-sm-6">
                                    <input id="birthday_date" type="text"
                                        class="form-control form-control-user datepicker @error('birthday_date') is-invalid @enderror"
                                        name="birthday_date" value="{{ old('birthday_date') }}" required
                                        autocomplete="birthday_date" placeholder="Tanggal Lahir">

                                    @error('birthday_date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <input id="address" type="text"
                                    class="form-control form-control-user @error('address') is-invalid @enderror"
                                    name="address" value="{{ old('address') }}" required autocomplete="address"
                                    placeholder="Alamat">

                                @error('address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <input id="showroom_name" type="text"
                                    class="form-control form-control-user @error('showroom_name') is-invalid @enderror"
                                    name="showroom_name" value="{{ old('showroom_name') }}" required
                                    autocomplete="showroom_name" placeholder="Nama Showroom">

                                @error('showroom_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary btn-user btn-block">
                                {{ __('Masukan Data') }}
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('prepend-style')
<link rel="stylesheet" href="{{ url('frontend/libraries/combined/css/gijgo.min.css') }}" />
@endpush

@push('addon-script')
<script src="{{ url ('frontend/libraries/combined/js/gijgo.min.js') }}"></script>
<script>
    $(document).ready(function() {
      $('.datepicker').datepicker({
          format: 'yyyy-mm-dd',
          uiLibrary: 'bootstrap4',
          icons : {
              rightIcon: '<img src="{{ url('frontend/images/ic_doe.png') }}" />'
          }
      })
    });
</script>
@endpush