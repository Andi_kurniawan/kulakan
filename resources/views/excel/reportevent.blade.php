<table>
    <thead>
        <tr>
            <th style="font-weight:bold;">No</th>
            <th style="font-weight:bold;">ID Event</th>
            <th style="font-weight:bold;">Agreement No</th>
            <th style="font-weight:bold;">Branch</th>
            <th style="font-weight:bold;">Product</th>
            <th style="font-weight:bold;">Category</th>
            <th style="font-weight:bold;">Status</th>
            <th style="font-weight:bold;">Asset Desc</th>
            <th style="font-weight:bold;">Year</th>
            <th style="font-weight:bold;">License Plate</th>
            <th style="font-weight:bold;">Chassis No</th>
            <th style="font-weight:bold;">Engine No</th>
            <th style="font-weight:bold;">Repo Date</th>
            <th style="font-weight:bold;">Aging Repo</th>
            <th style="font-weight:bold;">Inventory Date</th>
            <th style="font-weight:bold;">Aging Inventory</th>
            <th style="font-weight:bold;">Inventory Amount</th>
            <th style="font-weight:bold;">Bidder ID</th>
            <th style="font-weight:bold;">Bidder Name</th>
            <th style="font-weight:bold;">Price</th>
            <th style="font-weight:bold;">Transaction Status</th>
        </tr>
    </thead>
    <tbody>
        @foreach($event_details as $report)
        <tr>
            <td>{{ $report->id_event_details }}</td>
            <td>{{ $report->id_event_managements }}</td>
            <td>{{ $report->agreement_assets }}</td>
            <td>{{ $report->branch_assets }}</td>
            <td>{{ $report->product_assets }}</td>
            <td>{{ $report->category_assets }}</td>
            <td>{{ $report->status_assets }}</td>
            <td>{{ $report->name_assets }}</td>
            <td>{{ $report->year_assets }}</td>
            <td>{{ $report->plate_assets }}</td>
            <td>{{ $report->chassis_assets }}</td>
            <td>{{ $report->engine_assets }}</td>
            <td>{{ $report->repo_date_assets }}</td>
            <td>{{ $report->aging_repo_assets }}</td>
            <td>{{ $report->inv_date_assets }}</td>
            <td>{{ $report->aging_inv_assets }}</td>
            <td>{{ $report->amount_assets }}</td>
            <td>{{ $report->id_bidder }}</td>
            <td>{{ $report->name_bidder }}</td>
            <td>{{ $report->price }}</td>
            <td>{{ $report->status_transactions }}</td>
        </tr>
        @endforeach
    </tbody>
</table>