@extends('layouts.checkout')

@section('title', 'KULAKAN - Checkout Process')

@section('content')
<!-- MAIN -->
<main>
    <section class="main-container">
        <div class="container">
            <div class="pro-coloumn">
                <div class="category-products">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="section-success d-flex align-items-center">
                            <div class="col text-center">
                                @foreach ($items as $item)
                                    <img src="{{ url('frontend/images/ic_mail.png') }}" alt="" />
                                    <h1 class="mt-3">Transaksi Sukses</h1>
                                    <br>
                                    <h2>No. VA : 1234567890</h2>
                                    <br>
                                    <table class="table table-bordered" 
                                        style="width:550px;margin-left:240px;text-align:left;">
                                        <tr>
                                            <th>Nama Kendaraan</th>
                                            <td>{{ $item->asset_name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Plat Nomor</th>
                                            <td>{{ $item->license_plate }}</td>
                                        </tr>
                                    </table>
                                @endforeach
                                <p class="mt-4">
                                    Terima Kasih telah melakukan Transaksi
                                    <br />
                                    Silahkan lakukan Pembayaran
                                </p>
                                <a href="{{ route('home') }}" class="btn btn-home-page mt-3 mb-5 px-5">
                                    Lanjut Lelang
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection