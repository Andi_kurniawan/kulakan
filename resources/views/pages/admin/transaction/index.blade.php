@extends('layouts.admin')

@section('content')

<div class="breadcomb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-windows"></i>
                                </div>
                                <div class="breadcomb-ctn">
                                    <h2>Transaction Table</h2>
                                    <p>Welcome to Transaction Table</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="data-table-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="data-table-list">
                    <div class="table-responsive">
                        <table id="data-table-basic" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Event Location</th>
                                    <th>Agreement No</th>
                                    <th>Asset Name</th>
                                    <th>Bidder ID</th>
                                    <th>Bidder Name</th>
                                    <th>Price</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($items as $item)
                                <tr>
                                    <td>{{ $item->id_transactions }}</td>
                                    <td>{{ $item->warehouse_name }}</td>
                                    <td>{{ $item->agreement_assets }}</td>
                                    <td>{{ $item->name_assets }}</td>
                                    <td>{{ $item->id_users }}</td>
                                    <td>{{ $item->name_users }}</td>
                                    <td>Rp. {{ number_format($item->price, 0, ',', '.') }}</td>
                                    <td>{{ $item->status }}</td>
                                    <td>
        
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="9" class="text-center">
                                        Data Kosong
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
