@extends('layouts.admin')

@section('content')

<div class="breadcomb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-form"></i>
                                </div>
                                @foreach ($items as $item)
                                    <div class="breadcomb-ctn">
                                        <h2>Warehouse {{ $item->warehouse_name }}</h2>
                                        <p>Edit Warehouse {{ $item->warehouse_name }} Data</p>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if ($errors->any())
<div class=" container alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="form-element-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-element-list">
                    @foreach ($items as $item)
                        <form action="/admin/warehouse/update" method="POST" enctype="multipart/form-data">
                            @csrf
                            
                            <input type="hidden" class="form-control" placeholder required name="warehouse_id" value="{{ $item->warehouse_id }}" />

                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="nk-int-mk">
                                        <h2>Warehouse Photo</h2>
                                    </div>
                                    <div class="form-group">
                                        <div class="nk-int-st">
                                            <input type="file" class="form-control" name="warehouse_photo" style="padding:10px 12px;height:100%;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="nk-int-mk">
                                        <h2>Warehouse Name</h2>
                                    </div>
                                    <div class="form-group float-lb">
                                        <div class="nk-int-st">
                                            <input type="text" class="form-control" name="warehouse_name" placeholder="Name"
                                                value="{{ $item->warehouse_name }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="nk-int-mk">
                                        <h2>Warehouse City</h2>
                                    </div>
                                    <div class="form-group float-lb">
                                        <div class="nk-int-st">
                                            <input type="text" class="form-control" name="warehouse_city" placeholder="City"
                                                value="{{ $item->warehouse_city }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="nk-int-mk">
                                        <h2>Warehouse Address</h2>
                                    </div>
                                    <div class="form-group float-lb">
                                        <div class="nk-int-st">
                                            <textarea class="form-control" rows="5" name="warehouse_address"
                                                placeholder="Address">{{ $item->warehouse_address }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="nk-int-mk">
                                        <h2>Warehouse Head</h2>
                                    </div>
                                    <div class="form-group float-lb">
                                        <div class="nk-int-st">
                                            <input type="text" class="form-control" name="warehouse_head"
                                                placeholder="Warehouse Head" value="{{ $item->warehouse_head }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="nk-int-mk">
                                        <h2>Warehouse Number</h2>
                                    </div>
                                    <div class="form-group float-lb">
                                        <div class="nk-int-st">
                                            <input type="text" class="form-control" name="warehouse_number"
                                                placeholder="Warehouse Number" value="{{ $item->warehouse_number }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-example-int mg-t-15">
                                <button type="submit" class="btn btn-success notika-btn-success">Simpan</button>
                            </div>
                        </form>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('prepend-style')
<link rel="stylesheet" href="{{ url('backend/css/datapicker/datepicker3.css') }}">
@endpush

@push('addon-script')
<script src="{{ url('backend/js/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ url('backend/js/datapicker/datepicker-active.js') }}"></script>
@endpush