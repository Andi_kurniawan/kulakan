@extends('layouts.admin')

@section('content')

<div class="breadcomb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-windows"></i>
                                </div>
                                <div class="breadcomb-ctn">
                                    <h2>Warehouse Table</h2>
                                    <p>Welcome to Warehouse Table</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
                            <div class="breadcomb-report">
                                <a href="/admin/warehouse/create" class="btn">
                                    <i class="fa fa-plus fa-sm text-white-50"></i> Tambah Warehouse
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="data-table-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="data-table-list">
                    <div class="table-responsive">
                        <table id="data-table-basic" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Photo</th>
                                    <th>Name</th>
                                    <th>City</th>
                                    <th>Address</th>
                                    <th>Warehouse Head</th>
                                    <th>Phone Number</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($items as $item)
                                    <tr>
                                        <td>{{ $item->warehouse_id }}</td> 
                                        <td> 
                                            <img src="{{ url("storage/OpenHouse/Warehouse/$item->warehouse_photo") }}" alt="" class="img-thumbnail" style="width:150px;" />
                                        </td>
                                        <td>{{ $item->warehouse_name }}</td>
                                        <td>{{ $item->warehouse_city }}</td>
                                        <td>{{ $item->warehouse_address }}</td>
                                        <td>{{ $item->warehouse_head }}</td>
                                        <td>{{ $item->warehouse_number }}</td>
                                        <td>
                                            <a href="{{ URL('admin/warehouse/edit/' . Crypt::encrypt($item->warehouse_id) . '') }}" class="btn btn-primary">
                                                <i class="fa fa-edit"></i>
                                            </a> 
                                            <a href="/admin/warehouse/destroy/{{$item->warehouse_id}}" class="btn btn-danger">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="6" class="text-center">
                                            Data Kosong
                                        </td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection