@extends('layouts.admin')

@section('content')

<div class="breadcomb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-windows"></i>
                                </div>
                                <div class="breadcomb-ctn">
                                    <h2>Event Table</h2>
                                    <p>Welcome to Event Table</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
                            <div class="breadcomb-report">
                                <a href="{{ route('createManagement') }}" class="btn">
                                    <i class="fa fa-plus fa-sm text-white-50"></i> Tambah Event Management
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="data-table-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="data-table-list">
                    <div class="table-responsive">
                        <table id="data-table-basic" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Warehouse Name</th>
                                    <th>Warehouse City</th>
                                    <th>Warehouse Head</th>
                                    <th>Buy Now</th>
                                    <th>Open Event</th>
                                    <th>Close Event</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($items as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->warehouse_name }}</td>
                                        <td>{{ $item->warehouse_city }}</td>
                                        <td>{{ $item->warehouse_head }}</td>
                                        <td>
                                            @if ($item->buy_now!="0")
                                                <button disabled class="btn btn-info"><i class="fa fa-check"></i></button>
                                            @else
                                                <button disabled class="btn btn-danger"><i class="fa fa-times"></i></button>
                                            @endif
                                        </td>
                                        <td>{{ $item->open_event }}</td>
                                        <td>{{ $item->close_event }}</td>
                                        <td>
                                            <a href="{{ URL('admin/event-management/eventDetail/' . Crypt::encrypt($item->id) . '') }}" class="btn btn-info">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{ URL('admin/event-management/editManagement/' . Crypt::encrypt($item->id) . '') }}" class="btn btn-primary">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="/admin/event-management/destroyManagement/{{$item->id}}" class="btn btn-danger">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="8" class="text-center">
                                            Data Kosong
                                        </td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
