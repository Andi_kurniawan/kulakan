@extends('layouts.admin')

@section('content')
<div class="breadcomb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-list">
                    <div class="row">
                        @foreach ($items as $item)
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-windows"></i>
                                </div>
                                <div class="breadcomb-ctn">
                                    <h2>Event Detail</h2>
                                    <p>Event {{ $item->warehouse_name }} Detail</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
                            <div class="breadcomb-report">
                                <a href="/OpenHouseUpdate/{{$item->id}}" class="btn btn-sm btn-primary shadow-sm">
                                    <i class="fa fa-paper-plane fa-sm text-white-50"></i> Kirim Email
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if ($errors->any())
<div class="alert alert-danger container">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="data-table-list">
                <table class="table table-striped">
                    @foreach ($items as $item)
                    <tr>
                        <th width=170>Name</th>
                        <td>{{ $item->warehouse_name }}</td>
                    </tr>
                    <tr>
                        <th>City</th>
                        <td>{{ $item->warehouse_city }}</td>
                    </tr>
                    <tr>
                        <th>Address</th>
                        <td>{{ $item->warehouse_address }}</td>
                    </tr>
                    <tr>
                        <th>Warehouse Head</th>
                        <td>{{ $item->warehouse_head }}</td>
                    </tr>
                    <tr>
                        <th>Open Event</th>
                        <td>{{ $item->open_event }}</td>
                    </tr>
                    <tr>
                        <th>Close Event</th>
                        <td>{{ $item->close_event }}</td>
                    </tr>
                    <tr>
                        <th>Download Report</th>
                        <td>
                            <a href="{{ route('exportReport') }}" class="btn btn-sm btn-primary shadow-sm">
                                <i class="fa fa-print fa-sm text-white-50"></i> Download Report
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <th>Car</th>
                        <td>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0 20px 0 0;">
                                <div class="data-table-list" style="padding:20px 20px 0 0;">
                                    <div class="table-responsive">
                                        <table id="data-table-basic" class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Agreement No</th>
                                                    <th>License Plate</th>
                                                    <th>Asset Name</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($assets as $asset)
                                                <tr>
                                                    <td>{{ $asset->agreement_no }}</td>
                                                    <td>{{ $asset->license_plate }}</td>
                                                    <td>{{ $asset->asset_name }}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>

@endsection