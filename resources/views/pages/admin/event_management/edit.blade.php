@extends('layouts.admin')

@section('content')

<div class="breadcomb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-form"></i>
                                </div>
                                @foreach ($items as $item)
                                <div class="breadcomb-ctn">
                                    <h2>Event {{ $item->warehouse_name }}</h2>
                                    <p>Edit Event {{ $item->warehouse_name }} Data</p>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if ($errors->any())
<div class=" container alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="form-element-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-element-list">
                    @foreach ($items as $item)
                    <form action="{{ route('updateManagement')}}" method="POST">
                        @csrf

                        <input type="hidden" class="form-control" placeholder required name="id"
                            value="{{ $item->id }}" />

                        <table class="table table-bordered">
                            <tr>
                                <th>Warehouse Name</th>
                                <th>City</th>
                                <th>Address</th>
                                <th>Buy Now</th>
                            </tr>
                            <tr>
                                <td>{{ $item->warehouse_name }}</td>
                                <td>{{ $item->warehouse_city }}</td>
                                <td>{{ $item->warehouse_address }}</td>
                                <td>
                                    <div class="toggle-select-act fm-cmp-mg">
                                        <div class="nk-toggle-switch" data-ts-color="blue">
                                            <input type="hidden" name="buy_now" value="0">
                                            <input id="buy_now" type="checkbox" hidden="hidden" name="buy_now" value="{{ $item->buy_now ? '1' : '1' }}">
                                            <label for="buy_now" class="ts-helper"></label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Open Event</h2>
                                </div>
                                <div class="form-group nk-datapk-ctm form-elet-mg float-lb" id="data_1">
                                    <div class="input-group date nk-int-st">
                                        <span class="input-group-addon"></span>
                                        <input type="text" class="form-control" name="open_event"
                                            placeholder="Open Event" value="{{ $item->open_event }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Close Event</h2>
                                </div>
                                <div class="form-group nk-datapk-ctm form-elet-mg float-lb" id="data_1">
                                    <div class="input-group date nk-int-st">
                                        <span class="input-group-addon"></span>
                                        <input type="text" class="form-control" name="close_event"
                                            placeholder="Close Event" value="{{ $item->close_event }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-example-int mg-t-15">
                            <button type="submit" class="btn btn-success notika-btn-success">Simpan</button>
                        </div>
                    </form>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('prepend-style')
<link rel="stylesheet" href="{{ url('backend/css/datapicker/datepicker3.css') }}">
@endpush

@push('addon-script')
<script src="{{ url('backend/js/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ url('backend/js/datapicker/datepicker-active.js') }}"></script>
@endpush