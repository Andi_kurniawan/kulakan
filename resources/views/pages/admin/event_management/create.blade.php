@extends('layouts.admin')

@section('content')

<div class="breadcomb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-form"></i>
                                </div>
                                <div class="breadcomb-ctn">
                                    <h2>Event Management</h2>
                                    <p>Input New Event Data</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if ($errors->any())
<div class=" container alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="form-element-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-element-list">
                    <form action="{{ route('addManagement') }}" method="POST">
                        @csrf

                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Warehouse Name</h2>
                                </div>
                                <div class="bootstrap-select fm-cmp-mg float-lb" style="margin:15px 0 15px 0;">
                                    <select name="id_warehouses" class="selectpicker" data-live-search="true">
                                        <option value="">Select Warehouse ID</option>
                                        @foreach ($items as $item)
                                        <option value="{{ $item->warehouse_id }}">
                                            {{ $item->warehouse_name }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Open Event</h2>
                                </div>
                                <div class="form-group nk-datapk-ctm form-elet-mg float-lb" id="data_1">
                                    <div class="input-group date nk-int-st">
                                        <span class="input-group-addon"></span>
                                        <input type="text" class="form-control" name="open_event"
                                            placeholder="Open Event" value="{{ old('open_event') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="padding-bottom:20px;">
                                <div class="nk-int-mk">
                                    <h2>Close Event</h2>
                                </div>
                                <div class="form-group nk-datapk-ctm form-elet-mg float-lb" id="data_1">
                                    <div class="input-group date nk-int-st">
                                        <span class="input-group-addon"></span>
                                        <input type="text" class="form-control" name="close_event"
                                            placeholder="Close Event" value="{{ old('close_event') }}">
                                    </div>
                                </div>
                            </div>
                            <table class="table table-bordered">
                                <tr>
                                    <th style="padding-left:20px;">Asset List</th>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="data-table-list">
                                            <div class="table-responsive">
                                                <table id="data-table-basic" class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Agreement No</th>
                                                            <th>Asset Name</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($assets as $asset)
                                                        <tr>
                                                            <td>{{ $asset->agreement_no }}</td>
                                                            <td>{{ $asset->asset_name }}</td>
                                                            <td>
                                                                <div class="toggle-select-act sm-res-mg-t-10">
                                                                    <div class="nk-toggle-switch" data-ts-color="blue">
                                                                        <input type="checkbox" name="agreement_assets[]" id="{{ $asset->agreement_no }}" value="{{ $asset->agreement_no }}">
                                                                        <label for="ts3" class="ts-helper"></label>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="form-example-int mg-t-15">
                            <button type="submit" class="btn btn-success notika-btn-success">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('prepend-style')
<link rel="stylesheet" href="{{ url('backend/css/bootstrap-select/bootstrap-select.css') }}">
<link rel="stylesheet" href="{{ url('backend/css/datapicker/datepicker3.css') }}">
@endpush

@push('addon-script')
<script src="{{ url('backend/js/bootstrap-select/bootstrap-select.js') }}"></script>
<script src="{{ url('backend/js/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ url('backend/js/datapicker/datepicker-active.js') }}"></script>
@endpush