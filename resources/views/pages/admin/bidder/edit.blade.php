@extends('layouts.admin')

@section('content')
<div class="breadcomb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-form"></i>
                                </div>
                                @foreach ($items as $item)
                                    <div class="breadcomb-ctn">
                                        <h2>Bidder {{ $item->name }}</h2>
                                        <p>Edit Bidder {{ $item->name }} Data</p>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if ($errors->any())
<div class=" container alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="form-element-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-element-list">
                    @foreach ($items as $item)
                        <form action="{{ route('update-bidder') }}" method="POST">
                            @csrf
                            
                            <input type="hidden" class="form-control" placeholder required name="id" value="{{ $item->id }}" />

                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="nk-int-mk">
                                        <h2>Bidder Name</h2>
                                    </div>
                                    <div class="form-group float-lb">
                                        <div class="nk-int-st">
                                            <input type="text" class="form-control" name="name" placeholder="Name"
                                                value="{{ $item->name }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="nk-int-mk">
                                        <h2>Bidder Email</h2>
                                    </div>
                                    <div class="form-group float-lb">
                                        <div class="nk-int-st">
                                            <input type="text" class="form-control" name="email" placeholder="Email"
                                                value="{{ $item->email }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="nk-int-mk">
                                        <h2>Identity Number</h2>
                                    </div>
                                    <div class="form-group float-lb">
                                        <div class="nk-int-st">
                                            <input type="text" class="form-control" name="identity_number" placeholder="Identity Number"
                                                value="{{ $item->identity_number }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="nk-int-mk">
                                        <h2>Birthday Place</h2>
                                    </div>
                                    <div class="form-group float-lb">
                                        <div class="nk-int-st">
                                            <input type="text" class="form-control" name="birthday_place"
                                                placeholder="Birthday Place" value="{{ $item->birthday_place }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="nk-int-mk">
                                        <h2>Birthday Date</h2>
                                    </div>
                                    <div class="form-group nk-datapk-ctm form-elet-mg float-lb" id="data_1">
                                        <div class="input-group date nk-int-st">
                                            <span class="input-group-addon"></span>
                                            <input type="text" class="form-control" name="birthday_date" value="{{ $item->birthday_date }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="nk-int-mk">
                                        <h2>Bidder Address</h2>
                                    </div>
                                    <div class="form-group float-lb">
                                        <div class="nk-int-st">
                                            <textarea class="form-control" rows="5" name="address"
                                                placeholder="Address">{{ $item->address }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="nk-int-mk">
                                        <h2>Bidder KTP Address</h2>
                                    </div>
                                    <div class="form-group float-lb">
                                        <div class="nk-int-st">
                                            <textarea class="form-control" rows="5" name="ktp_address"
                                                placeholder="KTP Address">{{ $item->ktp_address }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="nk-int-mk">
                                        <h2>Phone Number</h2>
                                    </div>
                                    <div class="form-group float-lb">
                                        <div class="nk-int-st">
                                            <input type="text" class="form-control" name="phone_number"
                                                placeholder="Phone Number" value="{{ $item->phone_number }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="nk-int-mk">
                                        <h2>Showroom Name</h2>
                                    </div>
                                    <div class="form-group float-lb">
                                        <div class="nk-int-st">
                                            <input type="text" class="form-control" name="showroom_name"
                                                placeholder="Showroom Name" value="{{ $item->showroom_name }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-example-int mg-t-15">
                                <button type="submit" class="btn btn-success notika-btn-success">Simpan</button>
                            </div>
                        </form>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('prepend-style')
<link rel="stylesheet" href="{{ url('backend/css/datapicker/datepicker3.css') }}">
@endpush

@push('addon-script')
<script src="{{ url('backend/js/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ url('backend/js/datapicker/datepicker-active.js') }}"></script>
@endpush