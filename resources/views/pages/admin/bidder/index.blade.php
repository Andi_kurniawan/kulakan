@extends('layouts.admin')

@section('content')

<div class="breadcomb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-windows"></i>
                                </div>
                                <div class="breadcomb-ctn">
                                    <h2>Bidder Table</h2>
                                    <p>Welcome to Bidder Table</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="data-table-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="data-table-list">
                    <div class="table-responsive">
                        <table id="data-table-basic" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Identity Number</th>
                                    <th>Birthday</th>
                                    <th>Address</th>
                                    <th>Phone Number</th>
                                    <th>Showroom Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($items as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->identity_number }}</td>
                                    <td>{{ $item->birthday_place }}, {{ $item->birthday_date }}</td>
                                    <td>{{ $item->address }}</td>
                                    <td>{{ $item->phone_number }}</td>
                                    <td>{{ $item->showroom_name }}</td>
                                    <td>
                                        @if ($item->bidder_status=='VERIFIED')
                                            Approved
                                        @else
                                            New
                                        @endif
                                    </td>
                                    <td>
                                        @if ($item->bidder_status=='VERIFIED')
                                            <a href="{{ URL('admin/bidder/edit/' . Crypt::encrypt($item->id) . '') }}"
                                                class="btn btn-primary" title="Edit">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="/admin/bidder/destroy/{{$item->id}}" class="btn btn-danger" title="Delete">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        @else
                                            <form action="/admin/bidder/approveBidder/{{$item->id}}" method="POST">
                                                @csrf
                                                <input type="hidden" name="id" value="{{ $item->id }}">

                                                <button type="submit" class="btn btn-success">Approve</button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="9" class="text-center">
                                        Data Kosong
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection