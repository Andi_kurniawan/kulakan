@extends('layouts.admin')

@section('content')

<div class="breadcomb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-windows"></i>
                                </div>
                                @foreach ($assets as $asset)
                                    <div class="breadcomb-ctn">
                                        <h2>Negotiation List</h2>
                                        <p>Welcome to ({{ $asset->agreement_no }}) {{ $asset->asset_name }} Negotiation List</p>
                                    </div> 
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="data-table-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="data-table-list">
                    <div class="table-responsive">
                        <table id="data-table-basic" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Buyer ID</th>
                                    <th>Buyer Name</th>
                                    <th>Nego Time</th>
                                    <th>Nego Price</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($items as $item)
                                <tr>
                                    <td>{{ $item->id_transactions }}</td>
                                    <td>{{ $item->id_users }}</td>
                                    <td>{{ $item->name_users }}</td>
                                    <td>{{ \Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i:s')}}</td>
                                    <td>Rp. {{ number_format($item->price, 0, ',', '.') }}</td>
                                    <td>
                                        <form action="/admin/transaction/processNegotiation" method="POST">
                                            @csrf
        
                                            <input type="hidden" class="form-control" placeholder required name="id_transactions" value="{{ $item->id_transactions }}" />
                                            <input type="hidden" class="form-control" placeholder required name="agreement_assets" value="{{ $item->agreement_assets }}" />
        
                                            <button type="submit" class="btn btn-success" name="btnNegotiation">
                                                Approve
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="8" class="text-center">
                                        Data Kosong
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection