@extends('layouts.admin')

@section('content')

<div class="breadcomb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-windows"></i>
                                </div>
                                <div class="breadcomb-ctn">
                                    <h2>Negotiation Table</h2>
                                    <p>Welcome to Negotiation Table</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="data-table-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="data-table-list">
                    <div class="table-responsive">
                        <table id="data-table-basic" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Event Location</th>
                                    <th>Agreement No</th>
                                    <th>Asset Name</th>
                                    <th>License Plate</th>
                                    <th>Manufacturing Year</th>
                                    <th>Color</th>
                                    <th>Bottom Price</th>
                                    <th>Action</th>
                                </tr>
                            </thead> 
                            <tbody>
                                @forelse ($items as $item)
                                <tr>
                                    <td>{{ $item->warehouse_name }}</td>
                                    <td>{{ $item->agreement_assets }}</td>
                                    <td>{{ $item->name_assets }}</td>
                                    <td>{{ $item->license_plate }}</td>
                                    <td>{{ $item->manufacture_year }}</td>
                                    <td>{{ $item->color }}</td>
                                    <td>Rp. {{ number_format($item->bottom_price, 0, ',', '.') }}</td>
                                    <td>
                                        <a href="{{ URL('admin/transaction/negoDetail/' . Crypt::encrypt($item->agreement_assets) . '') }}"
                                            class="btn btn-success">View Bidding List</a>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="8" class="text-center">
                                        Data Kosong
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection