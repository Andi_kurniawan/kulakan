@extends('layouts.admin')

@section('content')

<div class="breadcomb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-form"></i>
                                </div>
                                <div class="breadcomb-ctn">
                                    <h2>Upload Asset</h2>
                                    <p>Upload Asset from STEAM</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if ($errors->any())
<div class=" container alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="form-element-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <form action="{{ route('addManagement') }}" method="POST">
                    @csrf
                    <div class="form-element-list">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Warehouse Name</h2>
                                </div>
                                <div class="bootstrap-select fm-cmp-mg float-lb" style="margin:15px 0 15px 0;">
                                    <select name="id_warehouses" class="selectpicker" data-live-search="true">
                                        <option value="">Select Warehouse ID</option>
                                        @foreach ($items as $item)
                                        <option value="{{ $item->warehouse_id }}">
                                            {{ $item->warehouse_name }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Puublishing From</h2>
                                </div>
                                <div class="form-group nk-datapk-ctm form-elet-mg float-lb" id="data_1">
                                    <div class="input-group date nk-int-st">
                                        <span class="input-group-addon"></span>
                                        <input type="text" class="form-control" name="publish_from"
                                            placeholder="Puublishing From" value="{{ old('publish_from') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="padding-bottom:20px;">
                                <div class="nk-int-mk">
                                    <h2>Publishing To</h2>
                                </div>
                                <div class="form-group nk-datapk-ctm form-elet-mg float-lb" id="data_1">
                                    <div class="input-group date nk-int-st">
                                        <span class="input-group-addon"></span>
                                        <input type="text" class="form-control" name="publish to"
                                            placeholder="Publishing To" value="{{ old('publish to') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-borderless">
                            <tr>
                                <td>
                                    <div class="data-table-list">
                                        <div class="table-responsive">
                                            <table id="data-table-basic" class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Agreement No</th>
                                                        <th>License Plate</th>
                                                        <th>Name</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @forelse ($assets as $asset)
                                                    <tr>
                                                        <td>{{ $asset->agreement_no }}</td>
                                                        <td>{{ $asset->license_plate }}</td>
                                                        <td>{{ $asset->asset_name }}</td>
                                                        <td>
                                                            <button type="button" class="btn btn-success" data-toggle="modal"
                                                                data-target="#addGroup-{{ $asset->agreement_no }}">Add to
                                                                Group</button>
                                                        </td>
                                                    </tr>
                                                    @empty
                                                    <tr>
                                                        <td colspan="10" class="text-center">
                                                            Data Kosong
                                                        </td>
                                                    </tr>
                                                    @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="data-table-list">
                                        <div class="table-responsive">
                                            <table id="data-table-basic" class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Group Name</th>
                                                        <th>Total Asset</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Kawasaki Ninja</td>
                                                        <td>8</td>
                                                        <td>
                                                            <a href="#" title="Delete Asset"
                                                                class="btn btn-danger">
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div class="form-example-int mg-t-15">
                            <button type="submit" class="btn btn-success btn-block notika-btn-success">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@foreach ($assets as $asset)
<div class="modal fade" id="addGroup-{{ $asset->agreement_no }}" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h2><strong>Ajust Price</strong></h2>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ route('ajustPrice') }}">
                    @csrf

                    <input type="hidden" class="form-control" required name="agreement_no"
                        value="{{ $asset->agreement_no }}" />

                    <div class="form-group">
                        <label for="asset_name">Asset Name</label>
                        <p>Rp. {{ $asset->asset_name }}</p>
                    </div>

                    <div class="bootstrap-select fm-cmp-mg float-lb" style="margin:15px 0 15px 0;">
                        <label for="available_group">Available Group</label>
                        <select name="id_warehouses" class="selectpicker" data-live-search="true">
                            <option value="">Select Group</option>
                            <option value="NINJA">
                                Kawasaki Ninja
                            </option>
                            <option value="CBR">
                                CBR 250
                            </option>
                        </select>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-default">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach

@endsection

@push('prepend-style')
<link rel="stylesheet" href="{{ url('backend/css/bootstrap-select/bootstrap-select.css') }}">
<link rel="stylesheet" href="{{ url('backend/css/datapicker/datepicker3.css') }}">
@endpush

@push('addon-script')
<script src="{{ url('backend/js/bootstrap-select/bootstrap-select.js') }}"></script>
<script src="{{ url('backend/js/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ url('backend/js/datapicker/datepicker-active.js') }}"></script>
@endpush