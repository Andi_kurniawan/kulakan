@extends('layouts.admin')

@section('content')

<div class="breadcomb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-form"></i>
                                </div>
                                @foreach ($items as $item)
                                <div class="breadcomb-ctn">
                                    <h2>Asset {{ $item->asset_name }}</h2>
                                    <p>Edit Asset {{ $item->asset_name }} Data</p>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if ($errors->any())
<div class=" container alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="form-element-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-element-list datepicker-int">
                    @foreach ($items as $item)
                    <form action="{{ route('update-asset')}}" method="POST">
                        @csrf

                        <input type="hidden" class="form-control" placeholder required name="id"
                            value="{{ $item->id }}" />

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Branch</h2>
                                </div>
                                <div class="form-group float-lb">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="branch" placeholder="Branch"
                                            value="{{ $item->branch }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Agreement_no</h2>
                                </div>
                                <div class="form-group float-lb">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="agreement_no"
                                            placeholder="Agreement No" value="{{ $item->agreement_no }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>License Plate</h2>
                                </div>
                                <div class="form-group float-lb">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="license_plate"
                                            placeholder="License Plate" value="{{ $item->license_plate }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Product</h2>
                                </div>
                                <div class="form-group float-lb">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="product" placeholder="Product"
                                            value="{{ $item->product }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Default Status</h2>
                                </div>
                                <div class="form-group float-lb">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="default_status"
                                            placeholder="Default Status" value="{{ $item->default_status }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Category</h2>
                                </div>
                                <div class="bootstrap-select fm-cmp-mg float-lb" style="margin-top:15px;">
                                    <select name="category" class="selectpicker">
                                        <option value="{{ $item->category }}">
                                            Select Category ( {{ $item->category }} )
                                        </option>
                                        <option value="MOBIL">MOBIL</option>
                                        <option value="MOTOR">MOTOR</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Asset Name</h2>
                                </div>
                                <div class="form-group float-lb">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="asset_name"
                                            placeholder="Asset Name" value="{{ $item->asset_name }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Chassis No</h2>
                                </div>
                                <div class="form-group float-lb">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="chassis_no"
                                            placeholder="Chassis No" value="{{ $item->chassis_no }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Engine No</h2>
                                </div>
                                <div class="form-group float-lb">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="engine_no" placeholder="Engine No"
                                            value="{{ $item->engine_no }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Manufacture Year</h2>
                                </div>
                                <div class="form-group float-lb">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="manufacture_year"
                                            placeholder="Manufacture Year" value="{{ $item->manufacture_year }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Color</h2>
                                </div>
                                <div class="form-group float-lb">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="color" placeholder="Color"
                                            value="{{ $item->color }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Transmission</h2>
                                </div>
                                <div class="form-group float-lb">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="transmission"
                                            placeholder="Transmission" value="{{ $item->transmission }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>STNK Validation</h2>
                                </div>
                                <div class="form-group nk-datapk-ctm form-elet-mg float-lb" id="data_1">
                                    <div class="input-group date nk-int-st">
                                        <span class="input-group-addon"></span>
                                        <input type="text" class="form-control" name="stnk_validation" value="{{ $item->stnk_validation }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Fuel</h2>
                                </div>
                                <div class="form-group float-lb">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="fuel"
                                            placeholder="Fuel" value="{{ $item->fuel }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Odometer</h2>
                                </div>
                                <div class="form-group float-lb">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="odometer" placeholder="Odometer"
                                            value="{{ $item->odometer }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Condition</h2>
                                </div>
                                <div class="form-group float-lb">
                                    <div class="nk-int-st">
                                        <textarea class="form-control" rows="5" name="condition"
                                            placeholder="Condition">{{ $item->condition }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Repossess Date</h2>
                                </div>
                                <div class="form-group nk-datapk-ctm form-elet-mg float-lb" id="data_1">
                                    <div class="input-group date nk-int-st">
                                        <span class="input-group-addon"></span>
                                        <input type="text" class="form-control" name="repossess_date" value="{{ $item->repossess_date }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Inventory Date</h2>
                                </div>
                                <div class="form-group nk-datapk-ctm form-elet-mg float-lb" id="data_1">
                                    <div class="input-group date nk-int-st">
                                        <span class="input-group-addon"></span>
                                        <input type="text" class="form-control" name="inventory_date" value="{{ $item->inventory_date }}">
                                    </div>
                                </div>
                            </div>
                        </div>                     
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Inventory Amount</h2>
                                </div>
                                <div class="form-group float-lb">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="inventory_amount"
                                            placeholder="Inventory Amount" value="{{ $item->inventory_amount }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Bottom Price</h2>
                                </div>
                                <div class="form-group float-lb">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="bottom_price"
                                            placeholder="Bottom Price" value="{{ $item->bottom_price }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Open Price</h2>
                                </div>
                                <div class="form-group float-lb">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="open_price"
                                            placeholder="Open Price" value="{{ $item->open_price }}">
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                        <div class="form-example-int mg-t-15">
                            <button type="submit" class="btn btn-success notika-btn-success">Simpan</button>
                        </div>
                    </form>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('prepend-style')
<link rel="stylesheet" href="{{ url('backend/css/bootstrap-select/bootstrap-select.css') }}">
<link rel="stylesheet" href="{{ url('backend/css/datapicker/datepicker3.css') }}">
@endpush

@push('addon-script')
<script src="{{ url('backend/js/bootstrap-select/bootstrap-select.js') }}"></script>
<script src="{{ url('backend/js/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ url('backend/js/datapicker/datepicker-active.js') }}"></script>
@endpush