@extends('layouts.admin')

@section('content')

<div class="breadcomb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-windows"></i>
                                </div>
                                @foreach ($items as $item)
                                <div class="breadcomb-ctn">
                                    <h2>Asset Detail</h2>
                                    <p>Asset {{ $item->asset_name }} Detail</p>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
                            <div class="breadcomb-report">
                                <button type="button" class="btn" data-toggle="modal" data-target="#importExcel"
                                    style="font-size:14px;height:31px;">
                                    <i class="fa fa-upload fa-sm text-white-50"></i> Import Asset
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="data-table-list">
                <table class="table table-striped">
                    @foreach ($items as $item)
                    <tr>
                        <th>Photo</th>
                        <td>
                            @forelse ($photos as $photo)
                                <img src="{{ asset("storage/OpenHouse/Asset/$photo->photo_assets")}}" alt=""
                                class="img-thumbnail" style="width:150px;margin-right:10px;" /> 
                            @empty
                                <p>Photo not available</p>
                            @endforelse
                        </td>
                    </tr>
                    <tr>
                        <th>Branch</th>
                        <td>{{ $item->branch }}</td>
                    </tr>
                    <tr>
                        <th>Agreement No</th>
                        <td>{{ $item->agreement_no }}</td>
                    </tr>
                    <tr>
                        <th>License Plate</th>
                        <td>{{ $item->license_plate }}</td>
                    </tr>
                    <tr>
                        <th>Product</th>
                        <td>{{ $item->product }}</td>
                    </tr>
                    <tr>
                        <th>Default Status</th>
                        <td>{{ $item->default_status }}</td>
                    </tr>
                    <tr>
                        <th>Category</th>
                        <td>{{ $item->category }}</td>
                    </tr>
                    <tr>
                        <th>Asset Name</th>
                        <td>{{ $item->asset_name }}</td>
                    </tr>
                    <tr>
                        <th>Chassis No</th>
                        <td>{{ $item->chassis_no }}</td>
                    </tr>
                    <tr>
                        <th>Engine No</th>
                        <td>{{ $item->engine_no }}</td>
                    </tr>
                    <tr>
                        <th>Manufacture Year</th>
                        <td>{{ $item->manufacture_year }}</td>
                    </tr>
                    <tr>
                        <th>Color</th>
                        <td>{{ $item->color }}</td>
                    </tr>
                    <tr>
                        <th>Transmission</th>
                        <td>{{ $item->transmission }}</td>
                    </tr>
                    <tr>
                        <th>STNK Validation</th>
                        <td>{{ $item->stnk_validation }}</td>
                    </tr>
                    <tr>
                        <th>Fuel</th>
                        <td>{{ $item->fuel }}</td>
                    </tr>
                    <tr>
                        <th>Odometer</th>
                        <td>{{ $item->odometer }}</td>
                    </tr>
                    <tr>
                        <th>Condition</th>
                        <td>{{ $item->condition }}</td>
                    </tr>
                    <tr>
                        <th>Repossess Date</th>
                        <td>{{ $item->repossess_date }}</td>
                    </tr>
                    <tr>
                        <th>Inventory Date</th>
                        <td>{{ $item->inventory_date }}</td>
                    </tr>
                    <tr>
                        <th>Aging Repo</th>
                        <td>{{ $item->aging_repo }}</td>
                    </tr>
                    <tr>
                        <th>Aging Inventory</th>
                        <td>{{ $item->aging_inv }}</td>
                    </tr>
                    <tr>
                        <th>Inventory Amount</th>
                        <td>{{ $item->inventory_amount }}</td>
                    </tr>
                    <tr>
                        <th>Price</th>
                        <td>
                            <table class="table table-bordered">
                                <tr>
                                    <th>Bottom Price</th>
                                    <th>Open Price</th>
                                </tr>
                                <tr>
                                    <td>Rp. {{ number_format($item->bottom_price, 0, ',', '.') }}</td>
                                    <td>Rp. {{ number_format($item->open_price, 0, ',', '.') }}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>

@endsection