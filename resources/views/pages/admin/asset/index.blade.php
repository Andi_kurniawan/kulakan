@extends('layouts.admin')

@section('content')

<div class="breadcomb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-windows"></i>
                                </div>
                                <div class="breadcomb-ctn">
                                    <h2>Asset Table</h2>
                                    <p>Welcome to Asset Table</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-3">
                            <div class="breadcomb-report">
                                <a href="{{ route('manageAsset') }}" class="btn" title="From STEAM"><i class="fa fa-folder"></i> Manage Asset</a>
                                <button type="button" class="btn" data-toggle="modal" data-target="#importExcel" title="CSV File">
                                    <i class="fa fa-file fa-sm text-white-50"></i> Import Asset
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="data-table-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="data-table-list">
                    <div class="table-responsive">
                        <table id="data-table-basic" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Agreement No</th>
                                    <th>License Plate</th>
                                    <th>Category</th>
                                    <th>Name</th>
                                    <th>Manufacturing Year</th>
                                    <th>Color</th>
                                    <th>Price</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($items as $item)
                                <tr>
                                    <td>{{ $item->agreement_no }}</td>
                                    <td>{{ $item->license_plate }}</td>
                                    <td>{{ $item->category }}</td>
                                    <td>{{ $item->asset_name }}</td>
                                    <td>{{ $item->manufacture_year }}</td>
                                    <td>{{ $item->color }}</td>
                                    <td>
                                        Rp. {{ number_format($item->open_price, 0, ',', '.') }}
                                    </td>
                                    <td>
                                        <a href="{{ URL('admin/asset/show/' . Crypt::encrypt($item->id) . '') }}"
                                            title="Asset Detail" class="btn btn-info">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        <a href="{{ URL('/admin/asset/managePhoto/' . Crypt::encrypt($item->id) . '') }}"
                                            title="Manage Photo" class="btn btn-success">
                                            <i class="fa fa-picture-o"></i>
                                        </a>
                                        <a href="{{ URL('admin/asset/edit/' . Crypt::encrypt($item->id) . '') }}"
                                            title="Edit Asset" class="btn btn-primary">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="/admin/asset/destroy/{{$item->id}}" title="Delete Asset"
                                            class="btn btn-danger">
                                            <i class="fa fa-trash"></i>
                                        </a> 
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="10" class="text-center">
                                        Data Kosong
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="post" action="{{ route('import-asset') }}" enctype="multipart/form-data">
            @csrf

            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label>Pilih file excel</label>
                    <div class="form-group">
                        <input type="file" name="file" required="required">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Import</button>
                </div>
            </div>
        </form>
    </div>
</div>

@foreach ($items as $item)
<div class="modal fade" id="ajustPrice-{{ $item->agreement_no }}" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h2>Ajust Price</h2>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ route('ajustPrice') }}">
                    @csrf

                    <input type="hidden" class="form-control" required name="agreement_no"
                        value="{{ $item->agreement_no }}" />

                    <div class="form-group">
                        <label for="price_assets">Price</label>
                        <p>Rp. {{ number_format($item->open_price, 0, ',', '.') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="price_ajusted">Price Ajusted</label>
                        <input type="number" class="form-control" name="price_ajusted" placeholder="Input Harga"
                            value="{{ old('price_ajusted') }}">
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-default">Ajust Price</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach

@endsection