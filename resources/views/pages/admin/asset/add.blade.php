@extends('layouts.admin')

@section('content')

<div class="breadcomb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-form"></i>
                                </div>
                                @foreach ($items as $item)
                                <div class="breadcomb-ctn">
                                    <h2>Asset {{ $item->asset_name }}</h2>
                                    <p>Input {{ $item->asset_name }} Photo</p>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if ($errors->any())
<div class=" container alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="form-element-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-element-list">
                    <h4>Available Image</h2>
                    <br>
                    <table class="table table-striped">
                        <tr>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                        @foreach ($images as $image)
                        <tr>
                            <td>
                                <a href="{{ asset("storage/OpenHouse/Asset/$image->photo_assets")}}">
                                    <img src="{{ asset("storage/OpenHouse/Asset/$image->photo_assets") }}"
                                        class="img-thumbnail" style="max-width:200px;max-height:200px;" >
                                </a>
                            </td>
                            <td>
                                <a href="/admin/asset/deletePhoto/{{$image->id_photos}}" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-element-list">
                    <h4>Add Image</h2>
                    <br>
                    <form action="{{ route('addPhoto') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        @foreach ($items as $item)

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Agreement No</h2>
                                </div>
                                <div class="form-group float-lb">
                                    <div class="nk-int-st">
                                        <input type="text" class="form-control" name="agreement_assets"
                                            placeholder="Agreement No" value="{{ $item->agreement_no }}" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="nk-int-mk">
                                    <h2>Asset Photo</h2>
                                </div>
                                <div class="form-group">
                                    <div class="nk-int-st">
                                        <input type="file" class="form-control" name="photo_asset" placeholder="Photo"
                                            style="padding:10px 12px;height:100%;">
                                    </div>
                                </div>
                                {{-- <div class="form-group">
                                        <div class="nk-int-st">
                                            <input type="file" class="form-control" name="photo_asset[]" placeholder="Photo" style="padding:10px 12px;height:100%;" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="nk-int-st">
                                            <input type="file" class="form-control" name="photo_asset[]" placeholder="Photo" style="padding:10px 12px;height:100%;" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="nk-int-st">
                                            <input type="file" class="form-control" name="photo_asset[]" placeholder="Photo" style="padding:10px 12px;height:100%;" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="nk-int-st">
                                            <input type="file" class="form-control" name="photo_asset[]" placeholder="Photo" style="padding:10px 12px;height:100%;" >
                                        </div>
                                    </div> --}}
                            </div>
                        </div>
                        @endforeach
                        <div class="form-example-int mg-t-15">
                            <button type="submit" class="btn btn-success notika-btn-success">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection