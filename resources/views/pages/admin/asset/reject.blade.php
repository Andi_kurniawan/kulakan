@extends('layouts.admin')

@section('content')

<div class="breadcomb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-windows"></i>
                                </div>
                                <div class="breadcomb-ctn">
                                    <h2>Asset Reject</h2>
                                    <p>Welcome to Asset Reject Table</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="data-table-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="data-table-list">
                    <div class="table-responsive">
                        <table id="data-table-basic" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Agreement No</th>
                                    <th>License Plate</th>
                                    <th>Name</th>
                                    <th>Manufacture Year</th>
                                    <th>Color</th>
                                    <th>Price</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($items as $item)
                                <tr>
                                    <td>{{ $item->agreement_no }}</td>
                                    <td>{{ $item->license_plate }}</td>
                                    <td>{{ $item->asset_name }}</td>
                                    <td>{{ $item->manufacture_year }}</td>
                                    <td>{{ $item->color }}</td>
                                    <td>
                                        Rp. {{ number_format($item->open_price, 0, ',', '.') }}
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-success" data-toggle="modal"
                                            data-target="#ajustPrice-{{ $item->agreement_no }}">Selling Back</button>
                                        <a href="#" class="btn btn-danger">Rejected</a>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="10" class="text-center">
                                        Data Kosong
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@foreach ($items as $item)
<div class="modal fade" id="ajustPrice-{{ $item->agreement_no }}" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h2><strong>Ajust Price</strong></h2>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ route('ajustPrice') }}">
                    @csrf

                    <input type="hidden" class="form-control" required name="agreement_no"
                        value="{{ $item->agreement_no }}" />

                    <div class="form-group">
                        <label for="asset_name">Asset Name</label>
                        <p>Rp. {{ $item->asset_name }}</p>
                    </div>

                    <div class="form-group">
                        <label for="price_assets">Price</label>
                        <p>Rp. {{ number_format($item->open_price, 0, ',', '.') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="price_ajusted">Price Ajusted</label>
                        <input type="number" class="form-control" name="price_ajusted" placeholder="Input Harga"
                            value="{{ old('price_ajusted') }}">
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-default">Ajust Price</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach

@endsection