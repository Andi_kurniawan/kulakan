@extends('layouts.checkout')

@section('title', 'KULAKAN - Checkout')

@section('content')
<!-- MAIN -->
<main>
    <section class="main-container">
        <div class="container">
            <div class="pro-coloumn">
                <div class="category-products">
                    <ol class="checkout-list" id="products-list">
                        @foreach ($items as $item)
                        <li class="item">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="product-image">
                                    <img class="small-image"
                                        src="{{ asset("storage/OpenHouse/Asset/$item->photo_asset")}}"
                                        alt="HTC Rhyme Sense">
                                </div>
                                <div class="product-shop">
                                    <h2 class="product-name">{{ $item->asset_name }}
                                    </h2>
                                    <div class="price-box">
                                        <span class="regular-price" id="product-price">
                                            <span class="price" style="color: #c90a0a;">
                                                Rp. {{ number_format($item->price, 0, ',', '.') }}
                                            </span>
                                        </span>
                                    </div>
                                    <div class="spec-row" id="summarySpecs">
                                        <table width="100%">
                                            <tbody>
                                                <tr>
                                                    <td class="label-spec">
                                                        Plat Nomor <span class="coln">:</span>
                                                    </td>
                                                    <td class="value-spec">{{ $item->license_plate }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="label-spec">
                                                        Tahun <span class="coln">:</span>
                                                    </td>
                                                    <td class="value-spec">{{ $item->manufacture_year }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="label-spec">
                                                        Warna <span class="coln">:</span>
                                                    </td>
                                                    <td class="value-spec">{{ $item->color }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="label-spec">
                                                        Transmisi <span class="coln">:</span>
                                                    </td>
                                                    <td class="value-spec">{{ $item->transmission }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="label-spec">
                                                        Masa Berlaku STNK <span class="coln">:</span>
                                                    </td>
                                                    <td class="value-spec">{{ $item->stnk_validation }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="label-spec">
                                                        Bahan Bakar <span class="coln">:</span>
                                                    </td>
                                                    <td class="value-spec">{{ $item->fuel }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="label-spec">
                                                        Engine No <span class="coln">:</span>
                                                    </td>
                                                    <td class="value-spec">{{ $item->engine_no }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="label-spec">
                                                        Chassis No <span class="coln">:</span>
                                                    </td>
                                                    <td class="value-spec">{{ $item->chassis_no }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="item" style="padding-bottom: 0;">
                            <table class="table table-borderless personal-info">
                                <tbody>
                                    <tr>
                                        <td width="320px"
                                            style="font-family: 'Saira Condensed', sans-serif;font-size:24px">
                                            Informasi Pribadi</td>
                                        <td>
                                            <table width="100%" class="table table-bordered">
                                                <tr>
                                                    <td width="160px">Nama</td>
                                                    <td>: {{ Auth::user()->name }}</td>
                                                </tr>
                                                <tr>
                                                    <td width="160px">Email</td>
                                                    <td>: {{ Auth::user()->email }}</td>
                                                </tr>
                                                <tr>
                                                    <td width="160px">Nomor Telpon</td>
                                                    <td>: {{ Auth::user()->phone_number }}</td>
                                                </tr>
                                                <tr>
                                                    <td width="160px">Alamat</td>
                                                    <td>: {{ Auth::user()->address }}</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </li>
                        <li class="item" style="padding-bottom: 0;">
                            <table class="table table-borderless pay-method">
                                <tbody>
                                    <tr>
                                        <td width="320px" style="font-size:24px"">Metode Pembayaran</td>
                                        <td>: <img src=" {{ url('frontend/images/bca-logo.png') }}" alt=""> BCA Virtual
                                            Account</td>
                                    </tr>
                                </tbody>
                            </table>
                        </li>
                        <li class="item">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="btn-go">
                                    <a href="/showCheckOutSuccess/{{$item->id_transactions}}" class="btn btn-success btn-lg">
                                        Lanjutkan Pembayaran
                                    </a>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection