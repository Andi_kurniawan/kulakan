@extends('layouts.checkout')

@section('title', 'KULAKAN - Asset Receive')

@section('content')
<!-- MAIN -->
<main>
    <section class="main-container">
        <div class="container">
            <div class="pro-coloumn">
                <div class="category-products">
                    <ol class="checkout-list" id="products-list" style="padding-left:0px;">
                        @foreach ($items as $item)
                        <li class="item row">
                            <div class="product-left" style="max-width:400px;">
                                <div class="product-image">
                                    <img class="small-image" src="{{ url('frontend/images/p1.jpg')}}"
                                        alt="HTC Rhyme Sense" style="width:400px;">
                                </div>
                            </div>
                            <div class="product-shop" style="max-width:660px;">
                                <h2 class="product-name">{{ $item->asset_name }}
                                </h2>
                                <div class="price-box">
                                    <span class="regular-price" id="product-price">
                                        <span class="price" style="color: #c90a0a;">
                                            Rp. {{ number_format($item->price, 0, ',', '.') }}
                                        </span>
                                    </span>
                                </div>
                                <div class="desc std">
                                    <table class="table table-striped" style="font-size:12px;">
                                        <tr>
                                            <td>Plat Nomor</td>
                                            <td>: {{ $item->license_plate }}</td>
                                            <td>Tahun</td>
                                            <td>: {{ $item->manufacture_year }}</td>
                                        </tr>
                                        <tr>
                                            <td>Warna</td>
                                            <td>: {{ $item->color }}</td>
                                            <td>Transmisi</td>
                                            <td>: {{ $item->transmission }}</td>
                                        </tr>
                                        <tr>
                                            <td>Masa STNK</td>
                                            <td>: {{ $item->stnk_validation }}</td>
                                            <td>Bensin</td>
                                            <td>: {{ $item->fuel }}</td>
                                        </tr>
                                        <tr>
                                            <td>Engine No</td>
                                            <td>: {{ $item->engine_no }}</td>
                                            <td>Chassis No</td>
                                            <td>: {{ $item->chassis_no }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </li>
                        <li class="item">
                            <div class="check-list">
                                <form action="{{ route('addCheckList') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="transaction_id" value="{{ $item->id_transactions }}">

                                    <div class="form-group input-group-sm mb-4" style="margin-left:20px;max-width:1045px;">
                                        <label style="font-size:12px;">Nama Penerima</label>
                                        <input type="text" name="receiver" class="form-control" placeholder="Nama Penerima"
                                            aria-label="Nama Penerima" aria-describedby="inputGroup-sizing-sm">
                                    </div>

                                    <div class="form-group row form-data" style="margin-left:20px;">
                                        <div style="width:80px;margin-right:20px;margin-bottom:15px;">
                                            <div class="custom-control custom-checkbox mr-sm-2">
                                                <input type="checkbox" class="custom-control-input" name="asset_checks"
                                                    id="asset_checks" value="AVAILABLE">
                                                <label class="custom-control-label" for="asset_checks" style="font-size:12px;">Asset</label>
                                            </div>
                                        </div>
                                        <div style="max-width:950px;width:100%;">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="asset_photos"
                                                    id="asset_photos">
                                                <label class="custom-file-label" for="asset_photos"
                                                    aria-describedby="inputGroupFileAddon02">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row form-data" style="margin-left:20px;">
                                        <div style="width:80px;margin-right:20px;margin-bottom:15px;">
                                            <div class="custom-control custom-checkbox mr-sm-2">
                                                <input type="checkbox" class="custom-control-input" name="bpkb_checks"
                                                    id="bpkb_checks" value="AVAILABLE">
                                                <label class="custom-control-label" for="bpkb_checks" style="font-size:12px;">BPKB</label>
                                            </div>
                                        </div>
                                        <div style="max-width:950px;width:100%;">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="bpkb_photos"
                                                    id="bpkb_photos">
                                                <label class="custom-file-label" for="bpkb_photos"
                                                    aria-describedby="inputGroupFileAddon02">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row form-data" style="margin-left:20px;">
                                        <div style="width:80px;margin-right:20px;margin-bottom:15px;">
                                            <div class="custom-control custom-checkbox mr-sm-2">
                                                <input type="checkbox" class="custom-control-input" name="stnk_checks"
                                                    id="stnk_checks" value="AVAILABLE">
                                                <label class="custom-control-label" for="stnk_checks" style="font-size:12px;">STNK</label>
                                            </div>
                                        </div>
                                        <div style="max-width:950px;width:100%;">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="stnk_photos"
                                                    id="stnk_photos">
                                                <label class="custom-file-label" for="stnk_photos"
                                                    aria-describedby="inputGroupFileAddon02">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row form-data" style="margin-left:20px;">
                                        <div style="width:80px;margin-right:20px;margin-bottom:15px;">
                                            <div class="custom-control custom-checkbox mr-sm-2">
                                                <input type="checkbox" class="custom-control-input" name="key_checks"
                                                    id="key_checks" value="AVAILABLE">
                                                <label class="custom-control-label" for="key_checks" style="font-size:12px;">KUNCI</label>
                                            </div>
                                        </div>
                                        <div style="max-width:950px;width:100%;">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="key_photos"
                                                    id="key_photos">
                                                <label class="custom-file-label" for="key_photos"
                                                    aria-describedby="inputGroupFileAddon02">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                    <label style="font-size:12px;margin-left:19px;">Dokumen Lainnya</label>
                                    <div class="form-group row form-data" style="margin-left:20px;">
                                        <div style="margin-right:20px;margin-bottom:15px;">
                                            <textarea name="other_checks" id="other_checks" cols="52"
                                                rows="5"></textarea>
                                        </div>
                                        <div style="max-width:635px;width:100%;">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="stnk_photos"
                                                    id="stnk_photos">
                                                <label class="custom-file-label" for="stnk_photos"
                                                    aria-describedby="inputGroupFileAddon02">Choose file</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row btn-check">
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                            <button type="submit" class="btn btn-btn btn-primary btn-lg">Simpan</button>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                            <a href="#" class="btn btn-btn btn-info btn-lg">Cetak</a>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                            <a href="/order" class="btn btn-btn btn-success btn-lg">Kembali</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection