@extends('layouts.app')

@section('title')
KULAKAN
@endsection

@section('content')

<!-- HEADER -->
<div id="carouselHeader" class="car-header carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100" src="{{ url('frontend/images/header1.jpg') }}" alt="First slide" />
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="{{ url('frontend/images/header2.jpg') }}" alt="Second slide" />
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="{{ url('frontend/images/header3.jpg') }}" alt="Third slide" />
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselHeader" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselHeader" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<!-- MAIN -->
<main>
    <!-- STATISTICS -->
    <div class="container">
        <section class="section-stats row justify-content-center" id="stats">
            <div class="col-3 col-md-2 stats-detail">
                <h2>{{ $user }}</h2>
                <p>Anggota</p>
            </div>
            <div class="col-3 col-md-2 stats-detail">
                <h2>{{ $warehouse }}</h2>
                <p>Warehouse</p>
            </div>
            <div class="col-3 col-md-2 stats-detail">
                <h2>{{ $asset }}</h2>
                <p>Kendaraan</p>
            </div>
            <div class="col-3 col-md-2 stats-detail">
                <h2>10</h2>
                <p>Partner</p>
            </div>
        </section>
    </div>

    <!-- POPULAR HEADING -->
    <section class="section-popular" id="popular">
        <div class="container">
            <div class="row">
                <div class="col text-center section-popular-heading">
                    <h2>Lokasi Open House</h2>
                    <p>
                        Ikuti open house di warehouse kami,
                        <br>
                        di seluruh indonesia
                        <br>
                        sekarang juga!
                    </p>
                </div>
            </div>
        </div>
    </section>

    <!-- POPULAR CONTENT -->
    <section class="section-popular-content" id="popularContent">
        <div class="container">
            <div class="section-popular-travel row justify-content-center">
                @foreach ($items as $item)
                <div class="col-sm-12 col-md-3 col-lg-3">
                    <div class="card-travel text-center d-flex flex-column"
                        style="background-image: url('{{ asset("storage/OpenHouse/Warehouse/$item->warehouse_photo")}}');">
                        <div class="travel-country">{{ $item->warehouse_city }}</div>
                        <div class="travel-location">{{ $item->warehouse_name }}</div>
                        <div class="travel-button mt-auto">
                            <a href="{{ URL('list/' . Crypt::encrypt($item->id) . '') }}"
                                class="btn btn-travel-details px-4">
                                Ikut Lelang
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

    <!-- TESTIMONIAL HEADING -->
    <section class="section-testimonial-heading" id="testimonialHeading">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <h2>Testimoni</h2>
                    <p>
                        Kami memberikan mereka
                        <br />
                        pengalaman terbaik
                    </p>
                </div>
            </div>
        </div>
    </section>

    <!-- TESTIMONIAL CONTENT -->
    <section class="section-testimonial-content" id="testimonialContent">
        <div class="container">
            <div class="section-popular-travel row justify-content-center">
                <div class="col-sm-6 col-md-6 col-lg-4">
                    <div class="card card-testimonial text-center">
                        <div class="testimonial-content">
                            <img src="{{ url('frontend/images/testi-1.png') }}" alt="User"
                                class="mb-4 rounded-circle" />
                            <h3 class="mb-4">Angga Risky</h3>
                            <p class="testimonial">
                                “ It was glorious and I could not stop to say wohooo for
                                every single moment Dankeeeeee “
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4">
                    <div class="card card-testimonial text-center">
                        <div class="testimonial-content">
                            <img src="{{ url('frontend/images/testi-2.png') }}" alt="User"
                                class="mb-4 rounded-circle" />
                            <h3 class="mb-4">Shayna</h3>
                            <p class="testimonial">
                                “ The trip was amazing and I saw something beautiful in that
                                Island that makes me want to learn more “
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4">
                    <div class="card card-testimonial text-center">
                        <div class="testimonial-content">
                            <img src="{{ url('frontend/images/testi-3.png') }}" alt="User"
                                class="mb-4 rounded-circle" />
                            <h3 class="mb-4">Shabrina</h3>
                            <p class="testimonial">
                                “ I loved it when the waves was shaking harder — I was
                                scared too “
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection

@push('prepend-style')

@endpush

@push('addon-script')

@endpush