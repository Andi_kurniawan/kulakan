@extends('layouts.app')

@section('title', 'KULAKAN - Lelang')

@section('content')
<div class="page-heading">
    <div class="page-title">
        @foreach ($warehouses as $warehouse)
        <h2>{{ $warehouse->warehouse_name }}</h2>
        @endforeach
    </div>
</div>

<!-- MAIN -->
@foreach ($items as $item)
<div class="modal fade bd-example-modal-lg" id="inputNego-{{ $item->agreement_no }}" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="post" action="{{ route('addNegotiation') }}">
            @csrf

            <div class="modal-content" style="font-family:'Saira Condensed', sans-serif;font-size:18px;">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalNego">Import Harga Nego</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="padding-left:25px;">
                    <input type="hidden" class="form-control" placeholder required name="id_event_managements"
                        value="{{ $item->id_event_managements }}" />
                    <input type="hidden" class="form-control" placeholder required name="id_users"
                        value="{{ Auth::user()->id }}" />
                    <input type="hidden" class="form-control" placeholder required name="name_users"
                        value="{{ Auth::user()->name }}" />
                    <input type="hidden" class="form-control" placeholder required name="agreement_assets"
                        value="{{ $item->agreement_no }}" />

                    <div class="form-group">
                        <label for="name_assets">Nama</label>
                        <input type="hidden" class="form-control" name="name_assets" value="{{ $item->asset_name }}">
                        <p>{{ $item->asset_name }}</p>
                    </div>
                    <div class="form-group">
                        <label for="license_plate">Plat Nomor</label>
                        <p>{{ $item->license_plate }}</p>
                    </div>
                    <div class="form-group">
                        <label for="price_assets">Harga</label>
                        <p>Rp. {{ number_format($item->bottom_price, 0, ',', '.') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="high_price">Penawaran Tertinggi</label>
                        <p>Rp. 100.000.000</p>
                    </div>
                    <div class="form-group">
                        <label for="nego_price">Harga Nego</label>
                        <input type="number" class="form-control" name="nego_price" placeholder="Input Harga"
                            value="{{ old('nego_price') }}">
                    </div>
                    <input type="hidden" class="form-control" placeholder required name="status"
                        value="MENUNGGU PERSETUJUAN" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Nego</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endforeach
<main>
    <section class="main-container">
        <div class="container">
            <div class="pro-coloumn">
                <div class="category-products">
                    <div class="row">
                        @foreach ($items as $item)
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mb-4">
                            <div class="card card-grid" style="max-width: 550px;">
                                <a href="{{ URL('list/indexDetail/' . Crypt::encrypt($item->agreement_no) . '') }}">
                                    @foreach ($p_ori as $original)
                                        <img class="card-img-top" src="{{ asset("storage/OpenHouse/Asset/$original->photo_assets")}}"
                                        alt="Card image cap">
                                    @endforeach
                                </a>
                                <div class="card-body card-text">
                                    <div class="row px-3">
                                        <a href="{{ URL('list/indexDetail/' . Crypt::encrypt($item->agreement_no) . '') }}">
                                            <h2>{{ $item->asset_name }}</h2>
                                        </a>
                                        <a href="{{ URL('list/indexDetail/' . Crypt::encrypt($item->agreement_no) . '') }}" class="btn btn-light ml-2 py-0"
                                            style="font-family:'Saira Condensed', sans-serif;height:26px;">View
                                            Detail</a>
                                    </div>
                                    <div class="card-price">
                                        <h2>Rp. {{ number_format($item->bottom_price, 0, ',', '.') }}</h2>
                                    </div>
                                </div>
                                <div class="table-spec">
                                    <table class="table table-hover">
                                        <tr>
                                            <td style="width:65px;">
                                                <img class="img-spec" src="{{ url('frontend/images/license_plate.png') }}" alt=""> 
                                            </td>
                                            <td style="font-weight:600;">Plat Nomor</td>
                                            <td>: {{ $item->license_plate }}</td>
                                        </tr>
                                        <tr>
                                            <td style="width:65px;">
                                                <img class="img-spec" src="{{ url('frontend/images/year.png') }}" alt=""> 
                                            </td>
                                            <td style="font-weight:600;">Tahun</td>
                                            <td>: {{ $item->manufacture_year }}</td>
                                        </tr>
                                        <tr>
                                            <td style="width:65px;">
                                                <img class="img-spec" src="{{ url('frontend/images/color.png') }}" alt=""> 
                                            </td>
                                            <td style="font-weight:600;">Warna</td>
                                            <td>: {{ $item->color }}</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="btn-go">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <button type="button" data-id="{!! $item->agreement_no !!}" data-toggle="modal"
                                            data-target="#inputNego-{{ $item->agreement_no }}"
                                            class="btn btn-success btn-lg">
                                            Nego
                                        </button>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        @if ($item->buy_now=="1")
                                        <form action="{{ route('addBuyNow') }}" method="POST">
                                            @csrf

                                            <input type="hidden" class="form-control" placeholder required
                                                name="id_event_managements" value="{{ $item->id_event_managements }}" />
                                            <input type="hidden" class="form-control" placeholder required
                                                name="agreement_assets" value="{{ $item->agreement_no }}" />
                                            <input type="hidden" class="form-control" placeholder required
                                                name="name_assets" value="{{ $item->asset_name }}" />
                                            <input type="hidden" class="form-control" placeholder required
                                                name="id_users" value="{{ Auth::user()->id }}" />
                                            <input type="hidden" class="form-control" placeholder required
                                                name="name_users" value="{{ Auth::user()->name }}" />
                                            <input type="hidden" class="form-control" placeholder required name="price"
                                                value="{{ $item->bottom_price }}" />


                                            <button type="submit" class="btn btn-warning btn-lg">
                                                Beli Sekarang
                                            </button>
                                        </form>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

@endsection

@push('prepend-style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
@endpush

@push('addon-script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script>
    persistent(
        $showConfirmButton = true, 
        $showCancelButton = false
    )
</script>
@endpush