@extends('layouts.app')

@section('title', 'KULAKAN - Lelang')

@section('content')
<div class="page-heading">
    <div class="page-title">
        @foreach ($items as $item)
        <h2>{{ $item->asset_name }}</h2>
        @endforeach
    </div>
</div>

<!-- MAIN -->
@foreach ($items as $item)
<div class="modal fade bd-example-modal-lg" id="inputNego-{{ $item->agreement_no }}" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="post" action="{{ route('addNegotiation') }}">
            @csrf

            <div class="modal-content" style="font-family:'Saira Condensed', sans-serif;font-size:18px;">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalNego">Import Harga Nego</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="padding-left:25px;">
                    <input type="hidden" class="form-control" placeholder required name="id_event_managements"
                        value="" />
                    <input type="hidden" class="form-control" placeholder required name="id_users"
                        value="{{ Auth::user()->id }}" />
                    <input type="hidden" class="form-control" placeholder required name="name_users"
                        value="{{ Auth::user()->name }}" />
                    <input type="hidden" class="form-control" placeholder required name="agreement_assets"
                        value="{{ $item->agreement_no }}" />

                    <div class="form-group">
                        <label for="name_assets">Nama</label>
                        <input type="hidden" class="form-control" name="name_assets" value="{{ $item->asset_name }}">
                        <p>{{ $item->asset_name }}</p>
                    </div>
                    <div class="form-group">
                        <label for="license_plate">Plat Nomor</label>
                        <p>{{ $item->license_plate }}</p>
                    </div>
                    <div class="form-group">
                        <label for="price_assets">Harga</label>
                        <p>Rp. {{ number_format($item->open_price, 0, ',', '.') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="nego_price">Harga Nego</label>
                        <input type="number" class="form-control" name="nego_price" placeholder="Input Harga"
                            value="{{ old('nego_price') }}">
                    </div>
                    <input type="hidden" class="form-control" placeholder required name="status"
                        value="MENUNGGU PERSETUJUAN" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Nego</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endforeach

<main>
    <section class="main-container">
        <div class="container">
            <div class="pro-coloumn">
                <div class="category-products">
                    <ol class="products-list" id="products-list">
                        @foreach ($items as $item)
                        <li class="item row"> 
                            <div class="product-left" style="width:500px;">
                                <div class="product-image xzoom-container mb-0">
                                    @foreach ($p_ori as $original)
                                        <img class="small-image xzoom"
                                        src="{{ asset("storage/OpenHouse/Asset/$original->photo_assets")}}"
                                        style="max-width:500px;" xoriginal="{{ asset("storage/OpenHouse/Asset/$original->photo_assets")}}">
                                    @endforeach
                                </div>
                                <div class="mb-2 mt-2 xzoom-thumbs">
                                    @foreach ($p_preview as $preview)
                                        <a href="{{ asset("storage/OpenHouse/Asset/$preview->photo_assets")}}">
                                            <img class="xzoom-gallery"
                                                src="{{ asset("storage/OpenHouse/Asset/$preview->photo_assets")}}"
                                                style="width:90px;" xpreview="{{ asset("storage/OpenHouse/Asset/$preview->photo_assets")}}">
                                        </a>
                                    @endforeach
                                </div>
                                <div class="btn-go" style="max-width:478px;">
                                    <button type="button" data-id="{!! $item->agreement_no !!}" data-toggle="modal"
                                        data-target="#inputNego-{{ $item->agreement_no }}"
                                        class="btn btn-success btn-lg ">
                                        Nego
                                    </button>
                                    <form action="{{ route('addBuyNow') }}" method="POST">
                                        @csrf

                                        <input type="hidden" class="form-control" placeholder required
                                            name="id_event_managements" value="" />
                                        <input type="hidden" class="form-control" placeholder required
                                            name="agreement_assets" value="{{ $item->agreement_no }}" />
                                        <input type="hidden" class="form-control" placeholder required
                                            name="name_assets" value="{{ $item->asset_name }}" />
                                        <input type="hidden" class="form-control" placeholder required name="id_users"
                                            value="{{ Auth::user()->id }}" />
                                        <input type="hidden" class="form-control" placeholder required name="name_users"
                                            value="{{ Auth::user()->name }}" />
                                        <input type="hidden" class="form-control" placeholder required name="price"
                                            value="{{ $item->open_price }}" />


                                        <button type="submit" class="btn btn-warning btn-lg">
                                            Beli Sekarang
                                        </button>
                                    </form>
                                </div>
                            </div>
                            <div class="product-shop" style="max-width:540px;">
                                <a href="#">
                                    <h2 class="product-name" style="font-size:32px;">{{ $item->asset_name }}</h2>
                                </a>
                                <div class="price-box">
                                    <span class="regular-price" id="product-price-159">
                                        <span class="price" style="color: #c90a0a;">Rp.
                                            {{ number_format($item->open_price, 0, ',', '.') }}</span>
                                    </span>
                                </div>
                                <div class="desc std"> 
                                    <h2 class="mt-4 mb-3 product-name" style="font-size:24px;">Spesifikasi</h2>
                                    <table class="table table-striped" style="font-size:12px;">
                                        <tr>
                                            <td>Plat Nomor</td>
                                            <td>: {{ $item->license_plate }}</td>
                                            <td>Tahun</td>
                                            <td>: {{ $item->manufacture_year }}</td>
                                        </tr>
                                        <tr>
                                            <td>Warna</td>
                                            <td>: {{ $item->color }}</td>
                                            <td>Transmisi</td>
                                            <td>: {{ $item->transmission }}</td>
                                        </tr>
                                        <tr>
                                            <td>Masa STNK</td>
                                            <td>: {{ $item->stnk_validation }}</td>
                                            <td>Bensin</td>
                                            <td>: {{ $item->fuel }}</td>
                                        </tr>
                                        <tr>
                                            <td>Engine No</td>
                                            <td>: {{ $item->engine_no }}</td>
                                            <td>Chassis No</td>
                                            <td>: {{ $item->chassis_no }}</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="desc std">
                                    <h2 class="mt-4 mb-3 product-name" style="font-size:24px;">Kondisi</h2>
                                    <p style="font-size:12px;">{{ $item->condition }}</p>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    </section>
</main>

@endsection

@push('prepend-style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
<link rel="stylesheet" href="{{ url('frontend/libraries/xzoom/xzoom.css') }}">
@endpush

@push('addon-script')
<script src="{{ url('frontend/libraries/xzoom/xzoom.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $('.xzoom, .xzoom-gallery').xzoom({
            zoomWidth: 400,
            title: false,
            tint: '#333',
            Xoffset: 10
        });
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script>
    persistent(
        $showConfirmButton = true, 
        $showCancelButton = false
    )
</script>
@endpush