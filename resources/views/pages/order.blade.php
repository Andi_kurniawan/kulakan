@extends('layouts.order')
 
@section('title', 'KULAKAN - Pesanan')

@section('content')
<!-- BANNER -->
<div class="page-heading">
    <div class="page-title">
        <h2>PESANAN SAYA</h2>
    </div>
</div>

<!-- MAIN -->
<main>
    <section class="main-container">
        <div class="container">
            <div class="pro-coloumn">
                <div class="order-row" id="summaryOrder">
                    <table width="100%" class="table table-borderless">
                        <thead class="thead-blue" style="text-align: center;">
                            <tr>
                                <th scope="col">NO</th>
                                <th scope="col">Foto</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody class="tbody-order">
                            @php $no = 1; @endphp
                            @forelse ($items as $item)
                                <tr>
                                    <th scope="row" class="order-no">{{ $no++ }}</th>
                                    <td class="order-image">
                                        {{-- <img src="{{ asset("storage/OpenHouse/Asset/$item->photo_asset")}}"
                                            alt="HTC Rhyme Sense"> --}}
                                    </td>
                                    <td class="order-name">
                                        {{ $item->asset_name }}
                                        <br>
                                        <p style="color:#c90a0a;font-size:24px;"> Rp. {{ number_format($item->price, 0, ',', '.') }}</p>
                                        <hr>
                                        <table class="table table-order ">
                                            <tbody>
                                                <tr>
                                                    <td>Plat Nomor</td>
                                                    <td>: {{ $item->license_plate }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tahun</td>
                                                    <td>: {{ $item->manufacture_year }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td class="order-status">
                                        {{ $item->status }}
                                        @if ($item->status=='MENUNGGU PEMBAYARAN')
                                            <br>
                                            <a href="{{ URL('list/showCheckOut/' . Crypt::encrypt($item->id_transactions) . '') }}" class="btn btn-info"
                                                style="padding:0 20px;font-size:20px;color:#fff;text-decoration:none;">CHECK OUT</a>
                                        @endif
                                        @if ($item->status=='PEMBAYARAN DIVERIFIKASI')
                                            <br>
                                            <a href="/list/showCheckList/{{$item->id_transactions}}" class="btn btn-success"
                                                style="padding:0 20px;font-size:20px;color:#fff;text-decoration:none;">ASSET RECEIVE</a>
                                        @endif
                                        <br>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4" class="text-center" style="padding-top:40px;font-size:26px;">
                                        Silahkan Lakukan Transaksi
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection