<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('photo_asset');
            $table->bigInteger('agreement_no');
            $table->string('branch');
            $table->string('product');
            $table->string('category');
            $table->string('default_status');
            $table->string('asset_name');
            $table->string('manufacture_year');
            $table->string('license_plate');
            $table->string('chassis_no');
            $table->string('engine_no');
            $table->date('repossess_date');
            $table->integer('aging_repo');
            $table->date('inventory_date');
            $table->integer('aging_inv');
            $table->double('inventory_amount', 20, 2);
            $table->bigInteger('bottom_price');
            $table->bigInteger('open_price');
            $table->string('color');
            $table->string('transmission');
            $table->date('stnk_validation');
            $table->string('fuel');
            $table->string('odometer');
            $table->longText('condition');
            $table->integer('flag')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
