<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheckAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('check_assets', function (Blueprint $table) {
            $table->bigIncrements('id_checks');
            $table->integer('asset_checks');
            $table->string('asset_photos', 255);
            $table->integer('bpkb_checks');
            $table->string('bpkb_photos', 255);
            $table->integer('stnk_checks');
            $table->string('stnk_photos', 255);
            $table->integer('key_checks');
            $table->string('key_photos', 255);
            $table->integer('other_checks');
            $table->string('other_photos', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_checks');
    }
}
