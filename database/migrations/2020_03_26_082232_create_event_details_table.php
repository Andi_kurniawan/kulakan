<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_details', function (Blueprint $table) {
            $table->bigIncrements('id_event_details');
            $table->integer('id_event_managements');
            $table->bigInteger('agreement_assets');
            $table->string('branch_assets');
            $table->string('product_assets');
            $table->string('category_assets');
            $table->string('status_assets');
            $table->string('name_assets');
            $table->string('year_assets');
            $table->string('plate_assets');
            $table->string('chassis_assets');
            $table->string('engine_assets');
            $table->date('repo_date_assets');
            $table->integer('aging_repo_assets');
            $table->date('inv_date_assets');
            $table->integer('aging_inv_assets');
            $table->double('amount_assets', 18, 2);
            $table->string('id_bidder');
            $table->string('name_bidder');
            $table->bigInteger('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_details');
    }
}
