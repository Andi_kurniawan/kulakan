<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $fillable = [
        'warehouse_id', 'warehouse_photo', 'warehouse_name', 'warehouse_city', 'warehouse_address', 'warehouse_head', 'phone_number'
    ];
}
