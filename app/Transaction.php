<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'id_transactions', 'id_event_managements', 'agreement_assets', 'name_assets', 'id_users', 'name_users', 'price', 'status'
    ];
}
