<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    public $table = 'assets';
    
    protected $fillable = [
        'id', 'photo_asset', 'branch', 'agreement_no', 'license_plate', 'product',
        'default_status', 'category', 'asset_name', 'chassis_no', 'engine_no',
        'manufacture_year', 'color', 'transmission', 'stnk_validation', 'fuel',
        'odometer', 'condition', 'asset_location', 'repossess_date', 
        'inventory_date', 'aging_repo', 'aging_inv', 'inventory_amount', 'bottom_price', 'open_price', 'flag' 
    ];

    public function asset_photos() {
        return $this->hasMany(AssetPhoto::class, 'agreement_assets', 'agreement_no');
    }
}
