<?php

namespace App\Imports;

use App\Asset;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;

class ImportAsset implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Asset([
            'photo_asset'       => $row[0],
            'agreement_no'      => $row[1],
            'branch'            => $row[2], 
            'product'           => $row[3],
            'category'          => $row[4],
            'default_status'    => $row[5], 
            'asset_name'        => $row[6],
            'manufacture_year'  => $row[7],
            'license_plate'     => $row[8], 
            'chassis_no'        => $row[9],
            'engine_no'         => $row[10],
            'repossess_date'    => $row[11], 
            'aging_repo'        => $row[12],
            'inventory_date'    => $row[13],
            'aging_inv'         => $row[14],
            'inventory_amount'  => $row[15],
            'bottom_price'      => $row[16], 
            'open_price'        => $row[17],
            'color'             => $row[18], 
            'transmission'      => $row[19],
            'stnk_validation'   => $row[20],
            'fuel'              => $row[21], 
            'odometer'          => $row[22],
            'condition'         => $row[23],
        ]);
    }
}
