<?php

namespace App\Exports;

use App\EventDetail;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

// use Maatwebsite\Excel\Concerns\FromCollection;
// use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportEvent implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */

    // public function collection()
    // {
    //     return EventDetail::all();
    // }

    // private $event_details;

    // public function __construct($event_details)
    // {
    //     $this->event_details = $event_details;
    // }

    // public function view(): View
    // {
    //     return view('excel.reportevent', [
    //         'event_details' => $this->event_details
    //     ]);
    // }

    public function view(): View
    {
        return view('excel.reportevent', [
            'event_details' => EventDetail::all()
        ]);
    }
}
