<?php

namespace App\Http\Controllers;

use App\User;
use App\Warehouse;
use App\Asset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class HomeController extends Controller
{
    public function welcome()
    {
        return view('welcome');
    }
    
    public function index()
    {
        $now = Carbon::now()->format('Y-m-d');

        $items = DB::table('event_managements')
            ->select('event_managements.id', 'warehouses.warehouse_id', 'warehouses.warehouse_photo', 'warehouses.warehouse_name', 'warehouses.warehouse_city', 
                    'warehouses.warehouse_address', 'warehouses.warehouse_head', 'event_managements.open_event', 'event_managements.close_event')
            ->join('warehouses', 'warehouses.warehouse_id', '=', 'event_managements.id_warehouses')
            ->where('event_managements.open_event', '<=', $now)
            ->where('event_managements.close_event', '>=', $now)
            ->get(); 

        return view('pages.home', [
            'items' => $items,
            'user' => User::count(),
            'warehouse' => Warehouse::count(),
            'asset' => Asset::count(),
        ]);
    }
}
