<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function index() 
    {
        $items = DB::table('transactions')
            ->select('transactions.id_transactions', 'assets.id', 'assets.asset_name', 
                    'assets.license_plate', 'assets.manufacture_year', 'transactions.price', 'transactions.status')
            ->join('assets', 'assets.agreement_no', '=', 'transactions.agreement_assets')
            ->get();

        return view('pages.order', [
            'items' => $items
        ]);
    }
}
