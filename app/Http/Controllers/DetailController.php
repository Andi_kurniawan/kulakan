<?php
 
namespace App\Http\Controllers;

use App\Asset;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt; 
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class DetailController extends Controller
{
    public function indexList($id)
    {
        $data = Crypt::decrypt($id);

        $items = DB::table('event_managements')
            ->select('event_assets.id_event_managements','assets.id', 'assets.agreement_no', 'assets.asset_name', 'assets.bottom_price', 
                    'assets.license_plate', 'assets.manufacture_year', 'assets.color', 'event_managements.buy_now') 
            ->join('event_assets', 'event_assets.id_event_managements', '=', 'event_managements.id')
            ->join('assets', 'assets.agreement_no', '=', 'event_assets.agreement_assets')
            ->where('event_managements.id', '=', $data)
            ->get();
 
        $warehouses = DB::table('event_managements')
            ->select('warehouses.warehouse_name')
            ->join('warehouses', 'warehouses.warehouse_id', '=', 'event_managements.id_warehouses')
            ->where('event_managements.id', '=', $data)
            ->get();

        $p_ori = DB::table('event_managements') 
            ->select('assets.id', 'asset_photos.photo_assets', 'asset_photos.created_at')
            ->join('event_assets', 'event_assets.id_event_managements', '=', 'event_managements.id')
            ->join('assets', 'assets.agreement_no', '=', 'event_assets.agreement_assets')
            ->join('asset_photos', 'asset_photos.agreement_assets', '=', 'assets.agreement_no')
            ->where('event_managements.id', '=', $data)
            ->orderBy('created_at', 'asc')
            ->limit(1)
            ->get();
        
        // $p_ori = Asset::with(['asset_photos'])->get();

        return view('pages.list', [
            'items' => $items,
            'warehouses' => $warehouses,
            'p_ori' => $p_ori
        ]);
    }

    public function indexDetail($agreement_no)
    {
        $data = Crypt::decrypt($agreement_no);

        $items = DB::table('event_managements')
            ->select('event_assets.id_event_managements','assets.id', 'assets.agreement_no', 'assets.asset_name', 'assets.open_price', 
                    'assets.license_plate', 'assets.manufacture_year', 'assets.color', 'assets.transmission', 'assets.stnk_validation', 
                    'assets.fuel', 'assets.engine_no', 'assets.chassis_no', 'assets.condition', 'event_managements.buy_now') 
            ->join('event_assets', 'event_assets.id_event_managements', '=', 'event_managements.id')
            ->join('assets', 'assets.agreement_no', '=', 'event_assets.agreement_assets')
            ->where('assets.agreement_no', '=', $data)
            ->get();

        $p_ori = DB::table('assets')
            ->select('assets.id', 'asset_photos.photo_assets', 'asset_photos.created_at')
            ->join('asset_photos', 'asset_photos.agreement_assets', '=', 'assets.agreement_no')
            ->where('assets.agreement_no', '=', $data)
            ->orderBy('created_at', 'asc')
            ->limit(1)
            ->get();

        $p_preview = DB::table('assets')
            ->select('assets.id', 'asset_photos.photo_assets')
            ->join('asset_photos', 'asset_photos.agreement_assets', '=', 'assets.agreement_no')
            ->where('assets.agreement_no', '=', $data)
            ->get();

        return view('pages.detail', [
            'items' => $items,
            'p_ori' => $p_ori,
            'p_preview' => $p_preview
        ]);
    }

    public function addNegotiation(Request $request)
    {
        $date = Carbon::now()->format('Y-m-d H:i:s', 'Indonesian/Jakarta');
 
        DB::table('transactions')->insert([
            'id_event_managements' => request()->id_event_managements,
            'agreement_assets' => request()->agreement_assets,
            'name_assets' => request()->name_assets,
            'id_users' => request()->id_users,
            'name_users' => request()->name_users,
            'price' => request()->nego_price,
            'status' => request()->status,
            'flag' => '0',
            'created_at' => $date,
            'updated_at' => $date
        ]);
        
        return back();
    }

    public function addBuyNow(Request $request)
    { 
        alert()->success('Sukses','Berhasil menambahkan produk')
            ->width('360px');

        DB::table('transactions')->insert([
            'id_event_managements' => request()->id_event_managements,
            'agreement_assets' => request()->agreement_assets,
            'name_assets' => request()->name_assets,
            'id_users' => request()->id_users,
            'name_users' => request()->name_users,
            'price' => request()->price,
            'flag' => '1',
            'status' => 'MENUNGGU PEMBAYARAN'
        ]);

        return back();
    }

    public function showCheckOut($id_transactions)
    {
        $data = Crypt::decrypt($id_transactions);

        $items = DB::table('transactions')
            ->select('transactions.id_transactions', 'assets.id', 'assets.photo_asset', 'assets.agreement_no', 'assets.asset_name', 'transactions.price', 
                    'assets.license_plate', 'assets.manufacture_year', 'assets.color', 'assets.transmission', 'assets.stnk_validation', 
                    'assets.fuel', 'assets.engine_no', 'assets.chassis_no')
            ->join('assets', 'assets.agreement_no', '=', 'transactions.agreement_assets')
            ->where('transactions.id_transactions', '=', $data)
            ->get();

        return view('pages.checkout', [
            'items' => $items,
        ]);
    }

    public function showCheckList($id_transactions)
    {
        $items = DB::table('transactions')
            ->select('transactions.id_transactions', 'assets.id', 'assets.agreement_no', 'assets.asset_name', 'transactions.price', 
                    'assets.license_plate', 'assets.manufacture_year', 'assets.color', 'assets.transmission', 'assets.stnk_validation', 
                    'assets.fuel', 'assets.engine_no', 'assets.chassis_no')
            ->join('assets', 'assets.agreement_no', '=', 'transactions.agreement_assets')
            ->where('transactions.id_transactions', '=', $id_transactions)
            ->get();

        return view('pages.checklist', [
            'items' => $items
        ]);
    }

    public function addCheckList(Request $request)
    {
        $date = Carbon::now()->format('Y-m-d H:i:s', 'Indonesian/Jakarta');

        $dateName = Carbon::now()->format('Y-m-d', 'Indonesian/Jakarta');
        
        $request->validate([
            'asset_photos'  => 'mimes:jpeg,png,jpg|max:2048',
            'bpkb_photos'   => 'mimes:jpeg,png,jpg|max:2048',
            'stnk_photos'   => 'mimes:jpeg,png,jpg|max:2048',
            'other_photos'  => 'mimes:jpeg,png,jpg|max:2048',
        ]);

        $asset_photos = $request->file('asset_photos');
        $asset_photos = str_replace(' ', '+', $asset_photos);       
        $assetName = 'Asset'.' - '.$dateName.' ('.mt_rand(10000,99999).')'.'.'.'png';
        Storage::disk('local')->put('public/OpenHouse/AssetReceive/'.$assetName, file_get_contents($asset_photos));

        $bpkb_photos = $request->file('asset_photos');
        $bpkb_photos = str_replace(' ', '+', $bpkb_photos);       
        $bpkbName = 'BPKB'.' - '.$dateName.' ('.mt_rand(10000,99999).')'.'.'.'png';
        Storage::disk('local')->put('public/OpenHouse/AssetReceive/'.$bpkbName, file_get_contents($bpkb_photos));

        $stnk_photos = $request->file('asset_photos');
        $stnk_photos = str_replace(' ', '+', $stnk_photos);       
        $stnkName = 'STNK'.' - '.$dateName.' ('.mt_rand(10000,99999).')'.'.'.'png';
        Storage::disk('local')->put('public/OpenHouse/AssetReceive/'.$stnkName, file_get_contents($stnk_photos));

        $key_photos = $request->file('asset_photos');
        $key_photos = str_replace(' ', '+', $key_photos);       
        $keyName = 'Key'.' - '.$dateName.' ('.mt_rand(10000,99999).') '.'.'.'png';
        Storage::disk('local')->put('public/OpenHouse/AssetReceive/'.$keyName, file_get_contents($key_photos));

        $other_photos = $request->file('asset_photos');
        $other_photos = str_replace(' ', '+', $other_photos);       
        $otherName = 'Other'.' - '.$dateName.' ('.mt_rand(10000,99999).') '.'.'.'png';
        Storage::disk('local')->put('public/OpenHouse/AssetReceive/'.$otherName, file_get_contents($other_photos));

        DB::table('check_assets')->insert([
            'transaction_id'    => request()->transaction_id,
            'receiver'          => request()->receiver,
            'asset_checks'      => request()->asset_checks,
            'asset_photos'      => $assetName,
            'bpkb_checks'       => request()->bpkb_checks,
            'bpkb_photos'       => $bpkbName,
            'stnk_checks'       => request()->stnk_checks,
            'stnk_photos'       => $stnkName,
            'key_checks'        => request()->key_checks,
            'key_photos'        => $keyName,
            'other_checks'      => request()->other_checks,
            'other_photos'      => $otherName,
            'created_at'        => $date    
        ]);

        return back();
    }

}
