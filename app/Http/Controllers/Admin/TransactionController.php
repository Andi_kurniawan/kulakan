<?php

namespace App\Http\Controllers\Admin;

use Mail;
use App\Http\Controllers\Controller;
use App\Transaction;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;

class TransactionController extends Controller
{

    public function indexTransaction()
    {
        $items = DB::table('transactions')
            ->select('transactions.id_transactions', 'warehouses.warehouse_name', 'transactions.agreement_assets', 'transactions.name_assets', 'transactions.id_users',
                    'transactions.name_users', 'transactions.price', 'transactions.status')
            ->join('event_managements', 'event_managements.id', '=', 'transactions.id_event_managements')
            ->join('warehouses', 'warehouses.warehouse_id', '=', 'event_managements.id_warehouses')
            ->get();
 
    	return view('pages.admin.transaction.index', [
            'items' => $items
        ]);
    }

    public function indexNegotiation()
    {
        $items = DB::table('transactions')
            ->select('warehouses.warehouse_name', 'transactions.agreement_assets', 'transactions.name_assets', 
                    'assets.license_plate', 'assets.manufacture_year', 'assets.color', 'assets.bottom_price', 'transactions.status')
            ->join('event_managements', 'event_managements.id', '=', 'transactions.id_event_managements')
            ->join('warehouses', 'warehouses.warehouse_id', '=', 'event_managements.id_warehouses')
            ->join('assets', 'assets.agreement_no', '=', 'transactions.agreement_assets')
            ->where('transactions.flag', '=', '0')
            ->where('transactions.status', '=', 'MENUNGGU PERSETUJUAN')
            ->groupBy('warehouses.warehouse_name', 'transactions.agreement_assets', 'transactions.name_assets', 
                    'assets.license_plate', 'assets.manufacture_year', 'assets.color', 'assets.bottom_price', 'transactions.status')
            ->get();
 
    	return view('pages.admin.negotiation.index', [
            'items' => $items
        ]);
    }

    public function negoDetail($agreement_assets)
    {
        $data = Crypt::decrypt($agreement_assets); 

        $items = DB::table('transactions')
            ->select('transactions.id_transactions', 'transactions.agreement_assets', 'transactions.name_assets', 'transactions.id_users',
                    'transactions.name_users', 'transactions.price', 'transactions.created_at')
            ->where('transactions.agreement_assets', '=', $data)
            ->where('transactions.flag', '=', '0')
            ->where('transactions.status', '=', 'MENUNGGU PERSETUJUAN')
            ->orderBy('transactions.price', 'desc')
            ->get();
        
        $assets = DB::table('transactions')
            ->select('assets.agreement_no', 'assets.asset_name')
            ->join('assets', 'assets.agreement_no', '=', 'transactions.agreement_assets')
            ->where('transactions.agreement_assets', '=', $data)
            ->groupBy('assets.agreement_no', 'assets.asset_name')
            ->get();
 
    	return view('pages.admin.negotiation.detail', [
            'items' => $items,
            'assets' => $assets
        ]);
    }

    public function processNegotiation(Request $request)
    {
        DB::table('transactions')
            ->where('agreement_assets',$request->agreement_assets)
            ->where('status','MENUNGGU PERSETUJUAN')
            ->update([
                'status' => 'PENAWARAN DITOLAK',
                'flag' => '0'
            ]);
                 
        DB::table('transactions')
            ->where('id_transactions',$request->id_transactions)
            ->update([
                'status' => 'MENUNGGU PEMBAYARAN',
                'flag' => '1' 
            ]); 

        $contents = DB::table('transactions')
            ->select('users.name', 'users.email', 'assets.asset_name', 'assets.license_plate', 'assets.manufacture_year', 'transactions.price')
            ->join('users', 'users.id', '=', 'transactions.id_users')
            ->join('assets', 'assets.agreement_no', '=', 'transactions.agreement_assets')
            ->where('transactions.id_transactions', '=', $request->id_transactions)
            ->get();

        foreach ($contents as $content) {
            Mail::send('email.negowin', [
                // 'photo_asset'=>$content->photo_asset, 
                'asset_name'=>$content->asset_name, 
                'license_plate'=>$content->license_plate,
                'manufacture_year'=>$content->manufacture_year, 
                'price'=>$content->price
            ],
            function ($message) use ($content) {
                $message->from('contact@kulakan.co.id', 'KULAKAN');
                $message->to($content->email, $content->name);
                $message->subject('Negotiation Win Notification');
                });
            }

        return back();
    }

}
