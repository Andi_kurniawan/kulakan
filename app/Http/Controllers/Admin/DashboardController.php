<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Asset;
use App\Transaction;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        return view('pages.admin.dashboard', [
            'user'                  => User::count(),
            'asset'                 => Asset::count(),
            'transaction_pending'   => Transaction::where('status', 'MENUNGGU PEMBAYARAN')->count(),
            'transaction_success'   => Transaction::where('status', 'TRANSAKSI SUKSES')->count(),
        ]);
    }
}
