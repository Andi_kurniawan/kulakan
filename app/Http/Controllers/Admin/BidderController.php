<?php

namespace App\Http\Controllers\Admin;

use Mail;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;

class BidderController extends Controller
{
    public function index()
    {
    	$items = DB::table('users')->get();
 
    	return view('pages.admin.bidder.index', ['items' => $items]);
    }

    public function edit($id)
    {
        $data = Crypt::decrypt($id);

        $items = DB::select("Select * from users where id = '$data'");
        
        return view('pages.admin.bidder.edit', ['items' => $items]);
    }

    public function update(Request $request)
    {
        DB::table('users')->where('id',$request->id)->update([
            'name' => request()->name,
            'email' => request()->email,
            'identity_number' => request()->identity_number,
            'birthday_place' => request()->birthday_place,
            'birthday_date' => request()->birthday_date,
            'ktp_address' => request()->ktp_address,
            'address' => request()->address,
            'phone_number' => request()->phone_number,
            'showroom_name' => request()->showroom_name
        ]);
        return redirect()->route('bidder');
    }

    public function approveBidder($id)
    {
        DB::table('users')->where('id',$id)->update([
            'bidder_status' => "VERIFIED"
        ]);

        $contents = DB::table('users')
            ->where('users.id', '=', $id)
            ->get();

        foreach ($contents as $content) {
            Mail::send('email.verifacc', [
                'id'=>$content->id, 
                'name'=>$content->name
            ],
            function ($message) use ($content) {
                $message->from('contact@kulakan.co.id', 'KULAKAN');
                $message->to($content->email, $content->name);
                $message->subject('Bidder Verification');
                });
            }

        return back();
    }

    public function destroy($id)
    {
        DB::table('users')->where('id',$id)->delete();

        return redirect()->route('bidder');
    }
}
