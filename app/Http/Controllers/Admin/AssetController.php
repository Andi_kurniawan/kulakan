<?php

namespace App\Http\Controllers\Admin;

use App\Asset;
use App\AssetPhoto;
use App\Imports\ImportAsset;
use App\Http\Controllers\Controller;
use App\Providers\AppServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class AssetController extends Controller
{
    public function index()
    {
    	$items = DB::table('assets')->get();
 
    	return view('pages.admin.asset.index', ['items' => $items]);
    }

    public function indexReject()
    {
    	$items = DB::table('assets')->get();
 
    	return view('pages.admin.asset.reject', ['items' => $items]);
    }

    public function import()
    {
        DB::table('assets')->truncate();

        Excel::import(new ImportAsset, request()->file('file'));
            
        return redirect()->route('asset');
    }

    public function managePhoto($id)
    {
        $data = Crypt::decrypt($id);

        $items = DB::table('assets')->where('id', '=', $data)->get();

        $images = DB::table('assets')
            ->select('assets.id', 'asset_photos.id_photos', 'asset_photos.photo_assets')
            ->join('asset_photos', 'asset_photos.agreement_assets', '=', 'assets.agreement_no')
            ->where('assets.id', '=', $data)
            ->get();

        return view('pages.admin.asset.add', [
            'items' => $items,
            'images' => $images
        ]);
    }

    public function addPhoto(Request $request)
    {
        $date = Carbon::now()->format('Y-m-d H:i:s', 'Indonesian/Jakarta');

        $request->validate([
            'photo_asset' => 'required|mimes:jpeg,png,jpg|max:2048',
        ]);
  
        $photo_asset = $request->file('photo_asset');
        
        $photo_asset = str_replace(' ', '+', $photo_asset);
        
        $photoName = mt_rand(10000,99999).'.'.'png';

        Storage::disk('local')->put('public/OpenHouse/Asset/'.$photoName, file_get_contents($photo_asset));

        DB::table('asset_photos')->insert([
            'agreement_assets' => request()->agreement_assets,
            'photo_assets' => $photoName,
            'created_at' => $date
        ]);

        return back()->with('success', 'Your images has been successfully');
    }

    public function deletePhoto($id_photos)
    {
        DB::table('asset_photos')->where('id_photos',$id_photos)->delete();

        return back()->with('success', 'Your photo has been deleted');
    }

    public function manageAsset()
    {
        $items = DB::table('warehouses')->get();

        $assets = DB::table('assets')->get();

        return view('pages.admin.asset.manage', [
            'items' => $items,
            'assets' => $assets
        ]);
    }

    public function ajustPrice(Request $request)
    {
        DB::table('assets')->where('agreement_no', $request->agreement_no)->update([
            'open_price' => request()->price_ajusted
        ]);

        return back();
    }

    public function show($id)
    {
        $data = Crypt::decrypt($id); 

        $items = DB::table('assets')
            ->where('assets.id', '=', $data)
            ->get();

        $photos = DB::table('assets')
                ->select('assets.id', 'asset_photos.photo_assets')
                ->join('asset_photos', 'asset_photos.agreement_assets', '=', 'assets.agreement_no')
                ->where('assets.id', '=', $data)
                ->get();
        
        return view('pages.admin.asset.detail', [
            'items' => $items,
            'photos' => $photos
        ]);
    }

    public function edit($id)
    {
        $data = Crypt::decrypt($id);

        $items = DB::select("Select * from assets where id = '$data'");
        
        return view('pages.admin.asset.edit', ['items' => $items]);
    }

    public function update(Request $request)
    {
        DB::table('assets')->where('id',$request->id)->update([
            'branch' => request()->branch,
            'agreement_no' => request()->agreement_no,
            'license_plate' => request()->license_plate,
            'product' => request()->product,
            'default_status' => request()->default_status,
            'category' => request()->category,
            'asset_name' => request()->asset_name,
            'chassis_no' => request()->chassis_no,
            'engine_no' => request()->engine_no,
            'manufacture_year' => request()->manufacture_year,
            'color' => request()->color,
            'transmission' => request()->transmission,
            'stnk_validation' => request()->stnk_validation,
            'fuel' => request()->fuel,
            'odometer' => request()->odometer,
            'condition' => request()->condition,
            'asset_location' => request()->asset_location,
            'repossess_date' => request()->repossess_date,
            'inventory_date' => request()->inventory_date,
            'inventory_amount' => request()->inventory_amount
        ]);
        return redirect()->route('asset');
    }

    public function destroy($id)
    {
        DB::table('assets')->where('id',$id)->delete();

        return redirect()->route('asset');
    }
}
