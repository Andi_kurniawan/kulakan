<?php

namespace App\Http\Controllers\Admin;

use Mail;
use App\EventManagement;
use App\EventAsset;
use App\Http\Controllers\Controller;
use App\Exports\ExportEvent;
use Illuminate\Support\Facades\DB; 
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;

class EventManagementController extends Controller
{

    public function index()
    {
        $items = DB::table('event_managements')
            ->select('event_managements.id', 'event_managements.id_warehouses', 'warehouses.warehouse_name', 
                    'warehouses.warehouse_city', 'warehouses.warehouse_address', 'warehouses.warehouse_head', 'event_managements.buy_now', 
                    'event_managements.open_event', 'event_managements.close_event')
            ->join('warehouses', 'warehouses.warehouse_id', '=', 'event_managements.id_warehouses')
            ->get();
 
    	return view('pages.admin.event_management.index', ['items' => $items]);
    }

    public function createManagement()
    {
        $items = DB::table('warehouses')->get();

        $assets = DB::table('assets')
            ->where('status_asset', '=', 'NEW')
            ->get();

        return view('pages.admin.event_management.create',  [
            'items' => $items,
            'assets' => $assets
        ]);
    }

    public function addManagement(Request $request)
    {
        DB::table('event_managements')->insert([
            'id_warehouses' => request()->id_warehouses,
            'open_event' => request()->open_event,
            'close_event' => request()->close_event
        ]);

        $id_event = DB::table('event_managements')->max('id');

        foreach ($request['agreement_assets'] as $item) {
            $asset[] = [
                'id_event_managements'  => $id_event,
                'agreement_assets'      => $item
            ];
        }

        EventAsset::insert($asset);

        $j = 0;
        $count1 = count($request['agreement_assets']);
        while($j < $count1){
            DB::table('assets')->where('agreement_no',$asset[$j]['agreement_assets'])
                ->update([
                    'status_asset' => "READY"
                ]);
            $j++;
        }

        return redirect()->route('event-management');
    }

    public function eventDetail($id)
    {
        $data = Crypt::decrypt($id);

        $items = DB::table('event_managements')
            ->select('event_managements.id', 'id_warehouses', 'warehouse_name', 'warehouse_city', 'warehouse_address', 'warehouse_head', 'open_event', 'close_event')
            ->join('warehouses', 'warehouses.warehouse_id', '=', 'event_managements.id_warehouses')
            ->where('event_managements.id', '=', $data)
            ->get();
        
        $assets = DB::table('event_assets')
            ->select('assets.agreement_no', 'assets.license_plate', 'assets.asset_name', 'assets.status_asset')
            ->join('assets', 'assets.agreement_no', '=', 'event_assets.agreement_assets')
            ->where('event_assets.id_event_managements', '=', $data)
            ->get();

        return view('pages.admin.event_management.detail', [
            'items'     => $items,
            'assets'    => $assets,
        ]);
    }

    public function exportReport(Request $request)
    {
        $fileName = 'OpenHouseReport_'.date('d-m-Y').'.xlsx';

        return Excel::download(new ExportEvent, $fileName);
    }

    function excel()
    {
        $event_data = DB::table('event_details')->get()->toArray();
        $event_array[] = array('ID Event', 'Agreement No', 'Branch', 'Product', 'Category');
        
        foreach($event_data as $event)
        {
            $event_array[] = array(
                'ID Event'      => $event->id_event_managements,
                'Agreement No'  => $event->agreement_assets,
                'Branch'        => $event->branch_assets,
                'Product'       => $event->product_assets,
                'Category'      => $event->category_assets
            );
        }

        Excel::create('Event Data', function($excel) use ($event_array) {
            $excel->setTitle('Event Data');
            $excel->sheet('Event Data', function($sheet) use ($event_array){
                $sheet->fromArray($event_array, null, 'A1', false, false);
            });
        })->download('xlsx');
    }

    public function editManagement($id)
    {
        $data = Crypt::decrypt($id);

        $items = DB::table('event_managements')
            ->select('event_managements.id', 'id_warehouses', 'warehouse_name', 'warehouse_city', 'warehouse_address','buy_now', 'open_event', 'close_event')
            ->join('warehouses', 'warehouses.warehouse_id', '=', 'event_managements.id_warehouses')
            ->where('event_managements.id', '=', $data)
            ->get();

        return view('pages.admin.event_management.edit', ['items' => $items]);
    }

    public function updateManagement(Request $request)
    {
        DB::table('event_managements')->where('id',$request->id)->update([
            'open_event' => request()->open_event,
            'close_event' => request()->close_event,
            'buy_now' => request()->buy_now
        ]);

        return redirect()->route('event-management');
    }

    public function OpenHouseUpdate($id)
    {
        $contents = DB::table('event_managements')
            ->select('event_managements.id', 'warehouses.warehouse_name', 'warehouses.warehouse_head', 'warehouses.warehouse_number', 
                    'event_managements.open_event', 'event_managements.close_event')
            // ->join('event_assets', 'event_assets.id_event_managements', '=', 'event_managements.id')
            // ->join('assets', 'assets.agreement_no', '=', 'event_assets.agreement_assets')
            ->join('warehouses', 'warehouses.warehouse_id', '=', 'event_managements.id_warehouses')
            ->where('event_managements.id', '=', $id)
            ->get();

        $users = DB::table('users')->get();

        foreach ($contents as $content) {
            Mail::send('email.openhouseupdate', [
                'warehouse_name'=>$content->warehouse_name, 
                'warehouse_head'=>$content->warehouse_head, 
                'warehouse_number'=>$content->warehouse_number,
                'open_event'=>$content->open_event, 
                'close_event'=>$content->close_event,
                // 'agreement_no'=>$content->agreement_no,
                // 'asset_name'=>$content->asset_name
            ],
            function ($message) use ($users) {
                $message->from('contact@kulakan.co.id');
                $message->to('romiz@gmail.com', 'Ahmad Romiz');
                $message->subject('Open House Information');
            });
        }

        return back();

        // return view('email.openhouseupdate', [
        //     'contents' => $contents,
        //     'users' => $users
        // ]);

    }

    public function destroyManagement($id)
    {
        DB::table('event_managements')->where('id', $id)->delete();

        DB::table('event_assets')->where('id_event_managements', $id)->delete();

        return redirect()->route('event-management');
    }
}
