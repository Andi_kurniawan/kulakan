<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Warehouse;
use Illuminate\Support\Facades\DB; 
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class WarehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$items = DB::table('warehouses')->get();
 
    	return view('pages.admin.warehouse.index', ['items' => $items]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.warehouse.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        $warehouse_photo = $request->file('warehouse_photo');
        
        $warehouse_photo = str_replace(' ', '+', $warehouse_photo);
        
        $photoName = mt_rand(10000,99999).'.'.'png';

        Storage::disk('local')->put('public/OpenHouse/Warehouse/'.$photoName, file_get_contents($warehouse_photo));

        DB::table('warehouses')->insert([
            'warehouse_photo' => $photoName,
            'warehouse_name' => request()->warehouse_name,
            'warehouse_city' => request()->warehouse_city,
            'warehouse_address' => request()->warehouse_address,
            'warehouse_head' => request()->warehouse_head,
            'warehouse_number' => request()->warehouse_number
        ]);
        
        return redirect()->route('warehouse');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($warehouse_id)
    {
        $data = Crypt::decrypt($warehouse_id);

        $items = DB::select("Select * from warehouses where warehouse_id = '$data'");
        
        return view('pages.admin.warehouse.edit', ['items' => $items]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'warehouse_photo' => 'required|mimes:jpeg,png,jpg|max:2048',
        ]);
  
        $warehouse_photo = $request->file('warehouse_photo');
        
        $warehouse_photo = str_replace(' ', '+', $warehouse_photo);
        
        $photoName = mt_rand(10000,99999).'.'.'png';

        Storage::disk('local')->put('public/OpenHouse/Warehouse/'.$photoName, file_get_contents($warehouse_photo));
        
        DB::table('warehouses')->where('warehouse_id',$request->warehouse_id)->update([
            'warehouse_photo' => $photoName,
            'warehouse_name' => request()->warehouse_name,
            'warehouse_city' => request()->warehouse_city,
            'warehouse_address' => request()->warehouse_address,
            'warehouse_head' => request()->warehouse_head,
            'warehouse_number' => request()->warehouse_number
        ]);
        
        return redirect()->route('warehouse');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($warehouse_id)
    {
        DB::table('warehouses')->where('warehouse_id',$warehouse_id)->delete();

        return redirect()->route('warehouse');
    }
}
