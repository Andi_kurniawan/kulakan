<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

class CheckoutController extends Controller
{
    public function showCheckOutSuccess($id_transactions) 
    {
        $items = DB::table('transactions')
            ->select('transactions.id_transactions', 'assets.asset_name', 'assets.license_plate')
            ->join('assets', 'assets.agreement_no', '=', 'transactions.agreement_assets')
            ->where('transactions.id_transactions', '=', $id_transactions)
            ->get();

        return view('pages.checkout-success', ['items' => $items]); 
    }
}
