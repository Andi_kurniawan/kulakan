<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetPhoto extends Model
{
    public $table = 'asset_photos';

    protected $fillable = [
        'id', 'agreement_assets', 'photo_assets'
    ];

    public function assets() {
        return $this->belongsTo(Asset::class, 'agreement_assets', 'agreement_no');
    }
}
