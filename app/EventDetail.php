<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventDetail extends Model
{
    public $table = 'event_details';

    public $fillable = [
        'id_event_details', 'id_event_managements', 'agreement_assets', 'branch_assets', 'product_assets',
        'category_assets', 'status_assets', 'name_assets', 'year_assets', 'plate_assets', 'chassis_assets',
        'engine_assets', 'repo_date_assets', 'aging_repo_assets', 'inv_date_assets', 'aging_inv_assets',
        'amount_assets', 'id_bidder', 'name_bidder', 'price'
    ];
}
