<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventManagement extends Model
{
    public $table = 'event_managements';
    
    protected $fillable = [
        'id_warehouses', 'name_warehouses', 'city_warehouses', 'open_event', 'close_event', 'buy_now'
    ];
}
