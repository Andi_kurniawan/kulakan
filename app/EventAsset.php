<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventAsset extends Model
{
    public $table = 'event_assets';

    protected $fillable = [
        'id', 'id_event_managements', 'agreement_assets'
    ];
}
